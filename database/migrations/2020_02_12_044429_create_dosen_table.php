<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nidn', 15)->nullable();
            $table->string('nama', 100);
            $table->text('alamat');
            $table->string('tempat_lahir', 50);
            $table->date('tanggal_lahir');
            $table->string('ktp', 20);
            $table->string('telepon', 15);
            $table->string('hp', 20);
            $table->string('email', 100);
            $table->string('website_personal', 100);
            $table->integer('jabatan_fungsional');
            $table->string('sinta_id', 15);
            $table->string('google_scholar', 20);
            $table->boolean('status')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_data');
    }
}
