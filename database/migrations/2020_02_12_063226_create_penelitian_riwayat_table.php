<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenelitianRiwayatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penelitian_riwayat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_penelitian');
            $table->boolean('step');
            $table->boolean('status')->nullable();
            $table->text('berkas');
            $table->text('keterangan');
            $table->unsignedTinyInteger('nilai')->nullable();
            $table->text('komentar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penelitian_riwayats');
    }
}
