<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenelitianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penelitian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_dosen', 15)->nullable();
            $table->text('judul')->nullable();
            $table->integer('skema')->nullable();
            $table->year('tahun_usulan')->nullable();
            $table->year('tahun_penelitian')->nullable();
            $table->text('abstrak')->nullable();
            $table->text('keyword')->nullable();
            $table->text('rumpun_ilmu')->nullable();
            $table->boolean('program_studi')->nullable();
            $table->string('kaprodi', 100)->nullable();
            $table->string('kalppm', 100)->nullable();
            $table->boolean('step')->nullable();
            $table->boolean('status')->nullable();
            $table->string('reviewer1', 15)->nullable();
            $table->string('reviewer2', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penelitians');
    }
}
