<?php

use Illuminate\Database\Seeder;
use App\Penelitian;

class PenelitianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penel = new Penelitian();
        $penel->judul = "penel1";
        $penel->tahun_usulan = 2020;
        $penel->lama_penelitian = 365;
        $penel->ringkasan = "aku sayang kamu";
        $penel->latar_belakang = "aku suka kamu";
        $penel->tinjauan_pustaka = "tapi kamu suka dia";
        $penel->metode = "pdkt";
        $penel->daftar_pustaka = "cinta yang diacuhkan";
        $penel->jadwal = "kapan aja bisa";
        $penel->status = 1;
        $penel->reviewer1 = "1";
        $penel->reviewer2 = "2";
        $penel->save();

        $penel = new Penelitian();
        $penel->judul = "penel2";
        $penel->tahun_usulan = 2020;
        $penel->lama_penelitian = 365;
        $penel->ringkasan = "aku sayang kamu";
        $penel->latar_belakang = "aku suka kamu";
        $penel->tinjauan_pustaka = "tapi kamu suka dia";
        $penel->metode = "pdkt";
        $penel->daftar_pustaka = "cinta yang diacuhkan";
        $penel->jadwal = "kapan aja bisa";
        $penel->status = 1;
        $penel->reviewer1 = "1";
        $penel->reviewer2 = "2";
        $penel->reviewer2 = "2";
        $penel->save();

        $penel = new Penelitian();
        $penel->judul = "penel3";
        $penel->tahun_usulan = 2020;
        $penel->lama_penelitian = 365;
        $penel->ringkasan = "aku sayang kamu";
        $penel->latar_belakang = "aku suka kamu";
        $penel->tinjauan_pustaka = "tapi kamu suka dia";
        $penel->metode = "pdkt";
        $penel->daftar_pustaka = "cinta yang diacuhkan";
        $penel->jadwal = "kapan aja bisa";
        $penel->status = 1;
        $penel->reviewer1 = "1";
        $penel->reviewer2 = "2";
        $penel->save();
        
    }
}
