<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'username' => '0100',
            'password' => Hash::make('0100')
        ])
            ->roles()
            ->attach(Role::where('name', 'admin')->first());

        User::create([
            'username' => '0101',
            'password' => Hash::make('0101')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0102',
            'password' => Hash::make('0102')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0103',
            'password' => Hash::make('0103')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0104',
            'password' => Hash::make('0104')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0105',
            'password' => Hash::make('0105')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0106',
            'password' => Hash::make('0106')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());

        User::create([
            'username' => '0107',
            'password' => Hash::make('0107')
        ])
            ->roles()
            ->attach(Role::where('name', 'dosen')->first());
    }
}
