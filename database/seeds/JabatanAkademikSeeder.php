<?php

use Illuminate\Database\Seeder;
use App\JabatanAkademik;

class JabatanAkademikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JabatanAkademik::create([
            'nama' => 'Asisten Ahli',
            'aktif' => 1
        ]);

        JabatanAkademik::create([
            'nama' => 'Lektor',
            'aktif' => 1
        ]);
        
        JabatanAkademik::create([
            'nama' => 'Lektor Kepala',
            'aktif' => 1
        ]);
        
        JabatanAkademik::create([
            'nama' => 'Profesor',
            'aktif' => 1
        ]);
        
        JabatanAkademik::create([
            'nama' => 'Tenaga Pengajar',
            'aktif' => 1
        ]);
    }
}
