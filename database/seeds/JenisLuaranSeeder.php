<?php

use Illuminate\Database\Seeder;
use App\JenisLuaran;

class JenisLuaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisLuaran::create([
            'nama' => 'Luaran Penelitian',
            'aktif' => 1,
            'status' => 1
        ]);

        JenisLuaran::create([
            'nama' => 'Publikasi Ilmiah',
            'aktif' => 1,
            'status' => 1
        ]);

        JenisLuaran::create([
            'nama' => 'Makalah',
            'aktif' => 1,
            'status' => 1
        ]);

        JenisLuaran::create([
            'nama' => 'HKI',
            'aktif' => 1,
            'status' => 1
        ]);

        JenisLuaran::create([
            'nama' => 'Buku Ajar',
            'aktif' => 1,
            'status' => 1
        ]);
    }
}
