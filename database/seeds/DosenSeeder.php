<?php

use Illuminate\Database\Seeder;
use App\Dosen;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dosen::create([
            'nidn' => '0100',
            'nama' => 'gusdek',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);

        Dosen::create([
            'nidn' => '0101',
            'nama' => 'gusmang',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0102',
            'nama' => 'swantika',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0103',
            'nama' => 'andy',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0104',
            'nama' => 'putri',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0105',
            'nama' => 'putra',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0106',
            'nama' => 'dewa',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
        
        Dosen::create([
            'nidn' => '0107',
            'nama' => 'dewi',
            'alamat' => 'rumah',
            'tempat_lahir' => 'denpasar',
            'tanggal_lahir' => '1999-01-01',
            'ktp' => '11111',
            'telepon' => '0361',
            'hp' => '081',
            'email' => 'gimail',
            'website_personal' => 'www',
            'jabatan_fungsional' => 1,
            'sinta_id' => '11',
            'google_scholar' => '12',
            'status' => 1
        ]);
    }
}
