<?php

use Illuminate\Database\Seeder;
use App\JenjangPendidikan;

class JenjangPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        JenjangPendidikan::create([
            'nama' => 'S-1',
            'aktif' => 1
        ]);
        
        JenjangPendidikan::create([
            'nama' => 'S-2',
            'aktif' => 1
        ]);
        
        JenjangPendidikan::create([
            'nama' => 'S-3',
            'aktif' => 1
        ]);
    }
}
