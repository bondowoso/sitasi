<?php

use App\RumpunIlmu;
use Illuminate\Database\Seeder;

class RumpunIlmuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RumpunIlmu::create([
            'nama' => 'Teknik Komputer',
            'aktif' => 1
        ]);

        RumpunIlmu::create([
            'nama' => 'Teknik Informatika',
            'aktif' => 1
        ]);

        RumpunIlmu::create([
            'nama' => 'Ilmu Komputer',
            'aktif' => 1
        ]);

        RumpunIlmu::create([
            'nama' => 'Sistem Informasi',
            'aktif' => 1
        ]);

        RumpunIlmu::create([
            'nama' => 'Teknologi Informasi',
            'aktif' => 1
        ]);

        RumpunIlmu::create([
            'nama' => 'Teknik Perangkat Lunak',
            'aktif' => 1
        ]);
    }
}
