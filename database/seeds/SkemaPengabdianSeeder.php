<?php

use Illuminate\Database\Seeder;
use App\SkemaPengabdian;

class SkemaPengabdianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SkemaPengabdian::create([
            'nama' => '2020 - Contoh Skema Pengabdian',
            'tanggal_usulan' => '2020-01-01',
            'tanggal_review' => '2020-02-01',
            'tanggal_laporan_kemajuan' => '2020-03-01',
            'tanggal_laporan_akhir' => '2020-04-01',
            'tanggal_publikasi' => '2020-05-01',
            'dana_maksimal' => '12000000',
            'status' => 'aktif'
        ]);
    }
}
