<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'description' => 'Administrator'
        ]);

        Role::create([
            'name' => 'pimpinan',
            'description' => 'Pimpinan LPPM'
        ]);

        Role::create([
            'name' => 'dosen',
            'description' => 'Dosen'
        ]);

        Role::create([
            'name' => 'penelitian',
            'description' => 'Koordinator Bidang Penelitian'
        ]);

        Role::create([
            'name' => 'pengabdian',
            'description' => 'Koordinator Bidang Pengabdian'
        ]);

        Role::create([
            'name' => 'reviewer',
            'description' => 'Reviewer'
        ]);
    }
}
