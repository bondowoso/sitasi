<?php

use Illuminate\Database\Seeder;
use App\ProgramStudi;

class ProgramStudiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProgramStudi::create([
            'nidn' => '0822088901',
            'nama' => 'Teknik Informatika',
            'kepala' => 'Wayan Gede Suka Parwita, M.Cs.',
            'aktif' => 1,
        ]);

        ProgramStudi::create([
            'nidn' => '0822088902',
            'nama' => 'Sistem Komputer',
            'kepala' => 'I Nyoman Buda Hartawan, M.Kom.',
            'aktif' => 1,
        ]);
    }
}
