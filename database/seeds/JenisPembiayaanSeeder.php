<?php

use Illuminate\Database\Seeder;
use App\JenisPembiayaan;

class JenisPembiayaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisPembiayaan::create([
            'nama' => 'Mandiri',
            'aktif' => 1
        ]);

        JenisPembiayaan::create([
            'nama' => 'Kampus',
            'aktif' => 1
        ]);
    }
}
