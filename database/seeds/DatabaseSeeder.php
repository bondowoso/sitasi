<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DosenSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ListRoleSeeder::class);
        $this->call(BidangPenelitianSeeder::class);
        $this->call(JabatanAkademikSeeder::class);
        $this->call(JenisLuaranSeeder::class);
        $this->call(JenisPembiayaanSeeder::class);
        $this->call(JenjangPendidikanSeeder::class);
        $this->call(SkemaPenelitianSeeder::class);
        $this->call(SkemaPengabdianSeeder::class);
        $this->call(ProgramStudiSeeder::class);
        $this->call(RumpunIlmuSeeder::class);
    }
}
