<?php

use Illuminate\Database\Seeder;
use App\BidangPenelitian;

class BidangPenelitianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BidangPenelitian::create([
            'nama' => 'Historis',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Survei',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Ex Post Facto',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Experimen',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Deskriptif',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Pengembangan',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Evaluasi',
            'aktif' => 1
        ]);

        BidangPenelitian::create([
            'nama' => 'Naturalistik',
            'aktif' => 1
        ]);
    }
}
