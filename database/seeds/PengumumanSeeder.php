<?php

use Illuminate\Database\Seeder;
use App\Pengumuman;
class PengumumanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengumum = new Pengumuman();
        $pengumum->judul = "pengumuman";
        $pengumum->deskripsi = "ini deskripsi";
        $pengumum->aktif = "1";
        $pengumum->tanggal_mulai = "2020-03-01 00:00:00";
        $pengumum->tanggal_selesai = "2020-03-02 00:00:00";
        $pengumum->save();

        $pengumum = new Pengumuman();
        $pengumum->judul = "pengumuman2";
        $pengumum->deskripsi = "ini deskripsi";
        $pengumum->aktif = "1";
        $pengumum->tanggal_mulai = "2020-04-01 00:00:00";
        $pengumum->tanggal_selesai = "2020-05-02 00:00:00";
        $pengumum->save();

        $pengumum = new Pengumuman();
        $pengumum->judul = "pengumuman3";
        $pengumum->deskripsi = "ini deskripsi";
        $pengumum->aktif = "1";
        $pengumum->tanggal_mulai = "2020-05-01 00:00:00";
        $pengumum->tanggal_selesai = "2020-06-02 00:00:00";
        $pengumum->save();
    }
}
