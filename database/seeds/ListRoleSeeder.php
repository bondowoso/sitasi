<?php

use Illuminate\Database\Seeder;
use App\ListRole;

class ListRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListRole::create([
            'role_id' => 1,
            'user_id' => 1
        ]);

        ListRole::create([
            'role_id' => 2,
            'user_id' => 2
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 2
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 3
        ]);

        ListRole::create([
            'role_id' => 6,
            'user_id' => 3
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 4
        ]);

        ListRole::create([
            'role_id' => 6,
            'user_id' => 4
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 5
        ]);

        ListRole::create([
            'role_id' => 4,
            'user_id' => 5
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 6
        ]);

        ListRole::create([
            'role_id' => 5,
            'user_id' => 6
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 7
        ]);

        ListRole::create([
            'role_id' => 3,
            'user_id' => 8
        ]);
    }
}
