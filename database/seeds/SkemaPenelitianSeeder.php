<?php

use Illuminate\Database\Seeder;
use App\SkemaPenelitian;
class SkemaPenelitianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SkemaPenelitian::create([
            'nama' => '2020 - Contoh Skema Penelitian',
            'tanggal_usulan' => '2020-01-01',
            'tanggal_review' => '2020-02-01',
            'tanggal_laporan_kemajuan' => '2020-03-01',
            'tanggal_laporan_akhir' => '2020-04-01',
            'tanggal_publikasi' => '2020-05-01',
            'dana_maksimal' => '12000000',
            'status' => 'aktif'
        ]);
        SkemaPenelitian::create([
            'nama' => '2021 - Contoh Skema Penelitian',
            'tanggal_usulan' => '2021-01-01',
            'tanggal_review' => '2021-02-01',
            'tanggal_laporan_kemajuan' => '2021-03-01',
            'tanggal_laporan_akhir' => '2021-04-01',
            'tanggal_publikasi' => '2021-05-01',
            'dana_maksimal' => '120000000',
            'status' => 'aktif'
        ]);
    }
}
