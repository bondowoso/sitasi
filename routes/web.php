<?php

use Illuminate\Support\Facades\Auth;

use App\ListRole;
use App\Penelitian;
use App\Pengabdian;
use App\RoleUser;
use App\SkemaPenelitian;
use App\SkemaPengabdian;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Using Closure based composers...
View::composer(['*'], function ($view) {
	if (isset(Auth::user()->id)) {
		$role = RoleUser::select('roles.description as description')
							->join('roles', 'role_user.role_id', 'roles.id')
							->firstWhere('role_user.user_id', Auth::user()->id);

		$roles = ListRole::select('list_roles.role_id as role_id', 'roles.description as description')
							->join('roles', 'list_roles.role_id', 'roles.id')
							->where('list_roles.user_id', Auth::user()->id)
							->get();
							
		$username = User::join('user_data', 'users.username', 'user_data.nidn')
							->select('user_data.nama', 'user_data.nidn')
							->firstWhere('users.id', Auth::user()->id);

		$rolecount = $roles->count();

        $skemaPenelitian = SkemaPenelitian::getSkemaByNIDN(Auth::user()->username);
        $skemaPengabdian = SkemaPengabdian::getSkemaByNIDN(Auth::user()->username);
										
		$view->with('skemaPenelitian', $skemaPenelitian);
		$view->with('skemaPengabdian', $skemaPengabdian);
		$view->with('role', $role);
		$view->with('roles', $roles);
		$view->with('rolecount', $rolecount);
		$view->with('username', $username);
	}
});

Route::get('tes', 'HomeController@test');

Route::get('/install','InstallController@install');
Auth::routes();
//testing upload read doc
Route::get('/doc','HomeController@docTest');
Route::post('/doc','HomeController@docProses');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/author', 'HomeController@author')->name('author');
Route::get('/penelitian/{id}', 'HomeController@firstPenelitian');
Route::get('/pengabdian/{id}', 'HomeController@firstPengabdian');
Route::get('/author', 'HomeController@author')->name('author');

Auth::routes();
Route::resource('/profil', 'ProfilController', [
	'only' => ['index', 'update']
]);
Route::get('/profil/edit', 'ProfilController@edit')->name('profil.edit');
Route::get('/skema/penelitian/{id}', 'HomeController@skemaPenelitian');
Route::get('/skema/pengabdian/{id}', 'HomeController@skemaPengabdian');


/*Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');
*/

Route::get('publications', 'Admin\GoogleScholarController@publications');
Route::get('latest-publication', 'Admin\GoogleScholarController@publication');
Route::get('statistics', 'Admin\GoogleScholarController@statistics');

Route::get('kepala-prodi', 'HomeController@getKepalaProdi')->name('getKepalaProdi');
Route::get('kepala-prodi/{id}', 'HomeController@firstKepalaProdi')->name('firstKepalaProdi');
Route::post('roles', 'HomeController@updateRole')->name('role_update');

Route::group(['prefix' => 'admin'], function()
{
	// semua url /admin/.. digroup sini
	//Route::get('/tes/{nidn}','Admin\PenelitianController@tes');
	Route::get('/', 'Admin\AdminController@index');

	Route::resource('penilaian', 'Admin\PenilaianController', ['only' => 'index'])->names(['index' => 'adminPenilaian.index']);

	Route::group(['prefix' => 'reviewer'], function () {
		Route::resource('/', 'Admin\ReviewerController', [
			'only' => ['index', 'store']
		])->names([
			'index' => 'adminReviewer.index',
			'store' => 'adminReviewer.store'
		]);
		Route::get('generate-penelitian', 'Admin\ReviewerController@generateReviewerPenelitian')->name('adminReviewer.generatePenelitian');
		Route::get('generate-pengabdian', 'Admin\ReviewerController@generateReviewerPengabdian')->name('adminReviewer.generatePengabdian');
	});

	Route::group(['prefix' => 'skema'], function()
	{
		Route::resource('/', 'Admin\SkemaController', ['only' => 'index'])->names(['index' => 'skema.index']);
		Route::get('/data-penelitian/{id}', 'Admin\AdminController@getskemapenelitian');
		Route::get('/data-pengabdian/{id}', 'Admin\AdminController@getskemapengabdian');
		Route::resource('/penelitian', 'Admin\SkemaPenelitianController', [
			'except' => 'index'
		])->names([
			'create' => 'skemapenelitian.create',
			'store' => 'skemapenelitian.store',
			'show' => 'skemapenelitian.show',
			'edit' => 'skemapenelitian.edit',
			'update' => 'skemapenelitian.update',
			'destroy' => 'skemapenelitian.destroy'
		]);
		Route::resource('/pengabdian', 'Admin\SkemaPengabdianController', [
			'except' => 'index'
		])->names([
			'create' => 'skemapengabdian.create',
			'store' => 'skemapengabdian.store',
			'show' => 'skemapengabdian.show',
			'edit' => 'skemapengabdian.edit',
			'update' => 'skemapengabdian.update',
			'destroy' => 'skemapengabdian.destroy'
		]);
	});

	Route::group(['prefix' => 'user'], function()
	{
		Route::get('/', 'Admin\UserController@index')->name('user.index');
		Route::post('/update-password', 'Admin\UserController@updatepassword')->name('admin.updatepassword');
		Route::post('/update-user', 'Admin\UserController@updateuser')->name('admin.updateuser');
	});

	Route::get('/data-dosen/{id}', 'Admin\AdminController@getDosen');
	Route::resource('/dosen', 'Admin\DosenController');
	Route::get('/reset-password/{id}', 'Admin\UserController@resetPassword')->name('admin.reset_password');
	Route::get('/role-user/{id}', 'Admin\AdminController@getRole');
	Route::group(['prefix' => 'penelitian'], function()
		{
			Route::get('/usulan','Admin\PenelitianController@getAllUsulan');
		}
	);
	Route::group(['prefix' => 'pengabdian'], function()
		{
			Route::get('/usulan','Admin\PengabdianController@getAllUsulan');
		}
	);
	
	Route::get('/data-usulan/penelitian/{id}', 'Admin\AdminController@getUsulanPenelitian');
	Route::get('/data-usulan/pengabdian/{id}', 'Admin\AdminController@getUsulanPengabdian');
	Route::get('/data-penilaian/{id}', 'Admin\PenilaianController@firstPenelitian');
	Route::get('/approver', function() {
    return view('admin.approver');
	});
});

Route::group(['prefix' => 'dosen'], function(){
	// semua url /dosen/.. digroup sini

	Route::get('/','Dosen\DosenController@index');
	Route::resource('/tanggungan', 'Dosen\TanggunganController');
	Route::get('/sinta', 'Dosen\DosenController@sinta')->name('dosen.sinta');
	Route::resource('/usulan', 'Dosen\UsulanController', [
		'only' => ['index', 'create', 'store', 'show', 'update']
	]);
	Route::resource('/pengabdian', 'Dosen\UsulanPengabdianController', [
		'only' => ['index', 'create', 'store', 'show', 'update']
	]);
});

Route::group(['prefix' => 'pimpinan'], function()
{
	// semua url /pimpinan/.. digroup sini

	Route::view('/', 'home.pimpinan');
	Route::get('/approver', function() {
    return view('pimpinan.approver');
	});

	Route::resource('penilaian', 'Admin\PenilaianController', ['only' => 'index'])->names(['index' => 'pimpinanPenilaian.index']);

	Route::group(['prefix' => 'reviewer'], function () {
		Route::resource('/', 'Admin\ReviewerController', [
			'only' => ['index', 'store']
		])->names([
			'index' => 'pimpinanReviewer.index',
			'store' => 'pimpinanReviewer.store'
		]);
		Route::get('generate-penelitian', 'Admin\ReviewerController@generateReviewerPenelitian')->name('pimpinanReviewer.generatePenelitian');
		Route::get('generate-pengabdian', 'Admin\ReviewerController@generateReviewerPengabdian')->name('pimpinanReviewer.generatePengabdian');
	});
});

Route::group(['prefix' => 'penelitian'], function()
{
	// semua url /penelitian/.. digroup sini

	Route::view('/', 'home.penelitian');
	Route::get('/pembagian-reviewer', function() {
    return view('penelitian.pembagian-reviewer');
	});
});

Route::group(['prefix' => 'pengabdian'], function()
{
	// semua url /pengabdian/.. digroup sini

	Route::view('/', 'home.pengabdian');
	Route::get('/pembagian-reviewer', function() {
    return view('pengabdian.pembagian-reviewer');
	});
});

Route::group(['prefix' => 'reviewer'], function()
{
	// semua url /reviewer/.. digroup sini

	Route::get('/', 'Reviewer\ReviewerController@index')->name('reviewerHome.index');
	Route::resource('/review', 'Reviewer\ReviewController', [
		'only' => ['index', 'store']
	])->names([
		'index' => 'reviewerReview.index', 'store' => 'reviewerReview.store'
	]);
	Route::get('/tes', function() {
    return view('reviewer.review');
	});
});