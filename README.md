# Readme
## Install

1. Clone projectnya
2. Jalankan 
```
composer install
```
3. Buat file .env copy dari env.example sesuaikan databasenya
4. Buat database di mysql
5. Jalankan di cmd
```
php artisan key:generate
```
6. Buka url localhost:8000/install untuk melakukan migrate dan membuat user admin