@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tahapan Usulan</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- column -->
        <div class="col">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <div class="card-title">
                @if ($form == 1)
                  Step 1 - Identitas Usulan
                @endif
                @if ($form == 2)
                  Step 2 - Atribut Usulan
                @endif
                @if ($form == 3)
                  Step 3 - Anggota Peneliti
                @endif
                @if ($form == 4)
                  Step 4 - Biaya Usulan
                @endif
                @if ($form == 5)
                  Step 5 - Isian Pengesahan 
                @endif
              </div>
            </div>
            <!-- form start -->
            @for ($i = 1; $i < 6; $i++)
                @if ($form == $i)
                    @include('dosen.usulan.form' . $i)
                @endif
            @endfor
            <!-- /.form end -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
@endsection