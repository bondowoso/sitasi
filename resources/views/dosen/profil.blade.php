@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Profil Pengguna</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Informasi Pribadi</h3>

              <div class="card-tools">
                <a href="{{ route('profil.edit') }}" class="btn btn-sm btn-success">Ubah Profil</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if(session()->get('success'))
                <div class ="alert alert-success">
                  {{ session()->get('success') }}  
                </div><br />
              @endif
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">No KTP:</p>
                    <p>{{ $dosen->ktp }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Alamat:</p>
                    <p>{{ $dosen->alamat }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">No Telepon:</p>
                    <p>{{ $dosen->telepon }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">No Handphone:</p>
                    <p>{{ $dosen->hp }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Email:</p>
                    <p>{{ $dosen->email }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Website Personal:</p>
                    <p>{{ $dosen->website_personal }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">NIDN:</p>
                    <p>{{ $dosen->nidn }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Jabatan Akademik:</p>
                    <p>{{ $jabatan_akademik->nama }}</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
@endsection