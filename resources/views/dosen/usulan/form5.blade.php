<form role="form" method="POST" action="{{ route('usulan.store') }}">
  @csrf
  <div class="card-body">
    <input type="hidden" name="step" value="5">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    <input type="hidden" name="id_penelitian" value="{{ $penelitian->id }}">
    <input type="hidden" name="skema" value="{{ $penelitian->skema }}">
    @if ($message = Session::get('skema'))
      <input type="hidden" name="skema" value="{{ $message }}">
    @endif
    <div class="form-group">
      <label for="">PROGRAM STUDI</label>
      <select class="form-control @error('program_studi') is-invalid @enderror program-studi" id="program_studi" name="program_studi" required autofocus>
        <option value="" hidden>--Pilih Program Studi</option>
        @foreach ($program_studi as $item)
          <option value={{$item->id}}
            @if (isset($penelitian))
              @if ($item->id == $penelitian->program_studi)
                  selected="selected"
              @endif
            @endif  
          >{{$item->nama}}</option>  
        @endforeach
      </select>

      @error('program_studi')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">KEPALA PROGRAM STUDI</label>
      <input class="form-control" type="text" id="kaprodi" name="kaprodi" value="" hidden>
      <input class="form-control" type="text" id="kaprodi2" name="" value="" disabled>
    </div>
    <div class="form-group">
      <label for="">KEPALA LPPM</label>
      <input class="form-control" type="text" id="kalppm" name="kalppm" value="{{ $kepala_lppm->nidn }}" hidden>
      <input class="form-control" type="text" id="" name="" value="{{ $kepala_lppm->nama }}" disabled>
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </form>
      <form action="{{ route('usulan.update', Auth::user()->username) }}" method="post">
        @csrf
        @method('PATCH')
        @if ($message = Session::get('skema'))
          <input type="hidden" name="skema" value="{{ $message }}">
        @endif
        <button type="submit" class="btn btn-danger float-right mr-2">Kembali</button>
      </form>
    </div>
  </div>