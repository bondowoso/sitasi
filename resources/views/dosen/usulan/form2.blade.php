<form role="form" method="POST" action="{{ route ('usulan.store') }}" enctype="multipart/form-data">
  @csrf
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
    @endif
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    <input type="hidden" name="id_penelitian" value="{{ $penelitian->id }}">
    @if ($message = Session::get('skema'))
      <input type="hidden" name="skema" value="{{ $message }}">
    @endif
    <div class="form-group">
      <label for="">JUDUL</label>
      <input class="form-control @error('judul') is-invalid @enderror" type="text" id="judul" name="judul"
      @if (isset($penelitian))  
        value="{{ $penelitian->judul }}"
      @endif
      required autofocus>

      @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">ABSTRAK</label>
      <textarea class="form-control @error('abstrak') is-invalid @enderror" name="abstrak" rows="5" required>@if (isset($penelitian)){{ $penelitian->abstrak }}@endif</textarea>

      @error('abstrak')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">KEYWORD</label>
      <input class="form-control @error('keyword') is-invalid @enderror" type="text" id="keyword" name="keyword"
      @if (isset($penelitian))
        value="{{ $penelitian->keyword }}"
      @endif
      required>

      @error('keyword')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">RUMPUN ILMU</label>
      <select class="form-control @error('rumpun_ilmu') is-invalid @enderror" id="rumpun_ilmu" name="rumpun_ilmu" required>
        <option value="" hidden>--Pilih Rumpun Ilmu</option>
        @foreach ($rumpunIlmu as $item)
          <option value="{{ $item->nama }}"
            @if (isset($penelitian))
              @if ($item->nama == $penelitian->rumpun_ilmu)
                  selected="selected"
              @endif
            @endif
          >{{ $item->nama }}</option>
        @endforeach
      </select>

      @error('rumpun_ilmu')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    {{-- <div class="form-group">
      <label for="">NAMA MITRA</label>
      <div class="row">
        <div class="col-8">
          <input class="form-control" type="text" id="" name="">
        </div>
        <div class="col-4">
          <button class="btn btn-success"><i class="nav-icon fas fa-plus"></i> Tambah Mitra</button>
        </div>
      </div>
    </div> --}}
    <div class="form-group">
      <label for="">PROPOSAL (.pdf, ukuran maks. 1 Mb)</label>
      @if (!$penelitian['berkas']->isEmpty())
        <object data="http://localhost:8000/{{ $penelitian['berkas'][0]->berkas }}" type="application/pdf" width="1000px" height="500px" style="max-width: 100%">
          <embed src="http://localhost:8000/{{ $penelitian['berkas'][0]->berkas }}" type="application/pdf">
              <p>This browser does not support PDFs</p>
        </object>
      @endif
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input @error('berkas') is-invalid @enderror" id="berkas" name="berkas">
          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
        </div>
      </div>
      
      @error('berkas')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </form>
    </div>
  </div>