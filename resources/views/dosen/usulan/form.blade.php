@extends('adminlte::page')

@section('usulan-open')
    menu-open
@endsection

@section('usulan-active')
    active
@endsection

@section('tambahusulan-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tambah Usulan</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- column -->
        <div class="col">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <div class="card-title">
                @empty ($usulan)
                Step 1 - Identitas Usulan
                @else
                  @if ($usulan->step == 0)
                    Step 1 - Identitas Usulan
                  @elseif ($usulan->step == 1)
                    Step 2 - Atribut Usulan
                  @elseif ($usulan->step == 2)
                    Step 3 - Anggota Peneliti
                  @elseif ($usulan->step == 3)
                    Step 4 - Biaya Usulan
                  @elseif ($usulan->step == 4)
                    Step 5 - Isian Pengesahan
                  @elseif ($usulan->step == 5)
                    Step 1 - Identitas Usulan
                  @endif
                @endempty
              </div>
            </div>
            <!-- form start -->
            @empty ($usulan)
              @include('dosen.usulan.form1')
            @else
              @if ($usulan->step == 5)
                @include('dosen.usulan.form1')
              @else
                @include('dosen.usulan.form' . ($usulan->step + 1))
              @endif
            @endempty
            <!-- /.form end -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script>
    // $(document).ready( function () {
    //   $('usulan').on('expanded.lte.treeview', handleExpandedEvent)
    // });
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      bsCustomFileInput.init();
    });
  </script>
  <script type="text/javascript">
    $(document).ready( function () {
      $( "select.program-studi" ).change(function() {
        var id = $(this).children("option:selected").val();
        $.get( "/kepala-prodi/" + id, function( data ) {
          console.log((data));
          var d = data;
          $('#kaprodi').val(d.nidn);
          $('#kaprodi2').val(d.kepala);
        });
        console.log($(this).children("option:selected").val());
      });
    } );
    $(document).ready( function () {
      $( "select.tahun-skema" ).change(function() {
        var id = $(this).children("option:selected").val();
        $.get( "/skema/penelitian/" + id, function( data ) {
          console.log((data));
          var d = data;
          $('#tahun_penelitian').val(d[0].tahun_usulan);
        });
        console.log($(this).children("option:selected").val());
      });
    } );
  </script>
@endsection