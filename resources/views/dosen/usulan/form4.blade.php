<form role="form" method="POST" action="{{ route('usulan.store') }}">
  @csrf
  <div class="card-body">
    <input type="hidden" name="step" value="4">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    <input type="hidden" name="id_penelitian" value="{{ $penelitian->id }}">
    <input type="hidden" name="skema" value="{{ $penelitian->skema }}">
    @if ($message = Session::get('skema'))
      <input type="hidden" name="skema" value="{{ $message }}">
    @endif
    <div class="form-group">
      <label for="usulan_dana">USULAN DANA</label>
      <input class="form-control @error('usulan_dana') is-invalid @enderror" type="number" id="usulan_dana" name="usulan_dana"
      @if (isset($penelitian['dana']))  
        value="{{ $penelitian['dana']->usulan_dana }}"
      @endif
      required autofocus>

      @error('usulan_dana')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="sumber_dana_lain">SUMBER DANA LAIN</label>
      <input class="form-control @error('sumber_dana_lain') is-invalid @enderror" type="number" id="sumber_dana_lain" name="sumber_dana_lain"
      @if (isset($penelitian['dana']))  
        value="{{ $penelitian['dana']->sumber_dana_lain }}"
      @endif
      required>

      @error('sumber_dana_lain')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </form>
      <form action="{{ route('usulan.update', Auth::user()->username) }}" method="post">
        @csrf
        @method('PATCH')
        @if ($message = Session::get('skema'))
          <input type="hidden" name="skema" value="{{ $message }}">
        @endif
        <button type="submit" class="btn btn-danger float-right mr-2">Kembali</button>
      </form>
    </div>
  </div>