<form role="form" method="POST" action="{{ route('usulan.store') }}">
  @csrf
  <div class="card-body">
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    <div class="form-group">
      <label for="">NAMA SKEMA</label>
      <select class="form-control @error('skema') is-invalid @enderror tahun-skema" id="skema" name="skema" required autofocus>
        <option value="" hidden>--Pilih Tahun Usulan</option>
        @foreach ($skemaPenelitian as $item)
          <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
      </select>

      @error('tahun_usulan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">TAHUN PENELITIAN</label>
      <input class="form-control @error('tahun_penelitian') is-invalid @enderror" type="text" id="tahun_penelitian" name="tahun_penelitian" value="{{ date('Y') }}" required>

      @error('tahun_penelitian')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </div>
  </div>
</form>