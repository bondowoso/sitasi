@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tambah Usulan</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">PENELITIAN</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="exampleInputFile">
                      <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>
                    <div class="input-group-append">
                      <span class="input-group-text" id="">Upload</span>
                    </div>
                  </div>
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
            <!-- form start -->
            <form role="form" method="POST" action="#">
              @csrf
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                <div class="site-section border-bottom mb-4">
                  <h5>  <i class="nav-icon fas fa-calendar"></i> Rencana Waktu</h5>
                </div>
                <form action="{{ route('usulan.store') }}" method="post">
                  <div class="form-group">
                    <label for="tahun_usulan">TAHUN USULAN</label>
                    <input class="form-control" type="text" id="tahun_usulan" name="tahun_usulan" value="2020" disabled>
                  </div>
                  <div class="form-group">
                    <label>TAHUN PELAKSANAAN</label>
                    <select class="form-control" id="tahun_pelaksanaan" name="tahun_pelaksanaan" required>
                      <option value="" hidden>--Pilih Tahun Usulan</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block mb-4">Daftar</button>
                </form>

                <div class="site-section">
                  <div class="mb-4">
                    <h5><i class="nav-icon fas fa-calendar"></i> Informasi Ketersediaan Skema</h5>
                  </div>
                  <ul class="nav flex-column" data-widget="treeview" role="usulan" data-accordion="false">
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Penelitian Dasar Unggulan Perguruan Tinggi <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-success mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Penelitian Pengembangan Unggulan Perguruan Tinggi <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Penelitian Terapan Unggulan Perguruan Tinggi <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Penelitian Dasar <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- /.card-body -->
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">PENGABDIAN</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="POST" action="#">
              @csrf
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                <div class="site-section border-bottom mb-4">
                  <h5>  <i class="nav-icon fas fa-calendar"></i> Rencana Waktu</h5>
                </div>
                <div class="form-group">
                  <label for="tahun_usulan">TAHUN USULAN</label>
                  <input class="form-control" type="text" id="tahun_usulan" name="tahun_usulan" value="2020" disabled>
                </div>
                <div class="form-group">
                  <label>TAHUN PELAKSANAAN</label>
                  <select class="form-control" id="tahun_pelaksanaan" name="tahun_pelaksanaan" required>
                    <option value="" hidden>--Pilih Tahun Usulan</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-primary btn-block mb-4">Daftar</button>

                <div class="site-section">
                  <div class="mb-4">
                    <h5><i class="nav-icon fas fa-calendar"></i> Informasi Ketersediaan Skema</h5>
                  </div>
                  <ul class="nav flex-column" data-widget="treeview" role="usulan" data-accordion="false">
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Program Pemberdayaan Masyarakat Unggulan Perguruan Tinggi <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-success mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            KKN Pembelajaran Pemberdayaan Masyarakat <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview ">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Program Kemitraan Masyarakat <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item has-treeview">
                      <div class="row">
                        <div class="col-10">
                          <a href="#">
                            Program Kemitraan Masyarakat Stimulus <i class="nav-icon fas fa-arrow-down"></i>
                          </a>
                        </div>
                        <div class="col-2">
                          <span class="float-right badge bg-danger mt-1">Tidak berhak</span>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- /.card-body -->
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script>
    $(document).ready( function () {
      $('usulan').on('expanded.lte.treeview', handleExpandedEvent)
    });
  </script>
@endsection