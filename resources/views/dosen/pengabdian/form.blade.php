@extends('adminlte::page')

@section('pengabdian-open')
    menu-open
@endsection

@section('pengabdian-active')
    active
@endsection

@section('tambahusulanpengabdian-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tambah Usulan Pengabdian</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- column -->
        <div class="col">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <div class="card-title">
              @empty($skema)
                <p class="text-secondary">Daftar Tanggungan yang masih berlaku</p>
                <div class ="alert alert-danger">
                  Pemberitahuan! Tidak ada skema yang bisa anda ikuti 
                </div><br />
              @else
                  @empty ($pengabdian)
                  Step 1 - Identitas Usulan
                  @else
                    @if ($pengabdian->step == 0)
                      Step 1 - Identitas Usulan
                    @elseif ($pengabdian->step == 1)
                      Step 2 - Atribut Usulan
                    @elseif ($pengabdian->step == 2)
                      Step 3 - Anggota Peneliti
                    @elseif ($pengabdian->step == 3)
                      Step 4 - Biaya Usulan
                    @elseif ($pengabdian->step == 4)
                      Step 5 - Isian Pengesahan
                    @elseif ($pengabdian->step == 5)
                      Step 1 - Identitas Usulan
                    @endif
                  @endempty
              </div>
            </div>
            <!-- form start -->
            @empty ($pengabdian)
              @include('dosen.pengabdian.form1')
            @else
              @if ($pengabdian->step == 5)
                @include('dosen.pengabdian.form1')
              @else
                @include('dosen.pengabdian.form' . ($pengabdian->step + 1))
              @endif
            @endempty
            @endempty
            <!-- /.form end -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      bsCustomFileInput.init();
    });
  </script>
  <script type="text/javascript">
    $(document).ready( function () {
      $( "select.program-studi" ).change(function() {
        var id = $(this).children("option:selected").val();
        $.get( "/kepala-prodi/" + id, function( data ) {
          console.log((data));
          var d = data;
          $('#kaprodi').val(d.nidn);
          $('#kaprodi2').val(d.kepala);
        });
        console.log($(this).children("option:selected").val());
      });
    } );
    $(document).ready( function () {
      $( "select.tahun-skema" ).change(function() {
        var id = $(this).children("option:selected").val();
        $.get( "/skema/pengabdian/" + id, function( data ) {
          console.log((data));
          var d = data;
          $('#tahun_pengabdian').val(d.tahun_usulan);
        });
        console.log($(this).children("option:selected").val());
      });
    } );
  </script>
@endsection