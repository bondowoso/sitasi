<form role="form" method="POST" action="{{ route('pengabdian.store') }}">
  @csrf
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div><br />
    @endif
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    @if ($message = Session::get('skema'))
      <input type="hidden" name="skema" value="{{ $message }}">
    @endif
    <div class="form-group">
      <label for="">NAMA SKEMA</label>
      <select class="form-control @error('tahun_usulan') is-invalid @enderror tahun-skema" id="tahun_usulan" name="skema" required autofocus>
        <option value="" hidden>--Pilih Tahun Usulan</option>
        @foreach ($skemaPengabdian as $item)
          <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
      </select>

      @error('tahun_usulan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="">TAHUN PENGABDIAN</label>
      <input class="form-control @error('tahun_pengabdian') is-invalid @enderror" type="text" id="tahun_pengabdian" name="tahun_pengabdian"
      @if (isset($pengabdian))
        value="{{ $pengabdian->tahun_pengabdian }}"
      @else
        value="{{ date('Y') }}"
      @endif
      required>

      @error('tahun_pengabdian')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </div>
  </div>
</form>