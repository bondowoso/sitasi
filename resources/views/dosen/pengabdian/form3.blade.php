<form role="form" method="POST" action="{{ route('pengabdian.store') }}">
  @csrf
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <input type="hidden" name="step" value="3">
    <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
    <input type="hidden" name="id_pengabdian" value="{{ $pengabdian->id }}">
    @if ($message = Session::get('skema'))
      <input type="hidden" name="skema" value="{{ $message }}">
    @else
      <input type="hidden" name="skema" value="{{ $pengabdian->skema }}">
    @endif
    <div class="form-group">
      <label for="">KETUA</label>
      <input class="form-control" type="text" id="dosen" name="dosen[]" value="{{ $profil->nama }}" disabled>
    </div>
    <div class="form-group" id="select-anggota">
      <label for="">ANGGOTA</label>
      <select class="form-control select1" id="select1" name="dosen[]" autofocus>
        <option value="" hidden>--Pilih dosen</option>
        @foreach ($dosen as $item)
          <option value="{{$item->nidn}}"
            @if (isset($penelitian))
              @if ($item->nama == $penelitian->tahun_usulan)
                  selected
              @endif
            @endif
          >{{$item->nama}}</option>
        @endforeach
      </select>
    </div>
    <button type="button" class="btn btn-success" id="tambah-anggota">Tambah</button>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <div class="card-tools">
      <button type="submit" class="btn btn-primary float-right">Selanjutnya</button>
    </form>
      <form action="{{ route('pengabdian.update', Auth::user()->username) }}" method="post">
        @csrf
        @method('PATCH')
        @if ($message = Session::get('skema'))
          <input type="hidden" name="skema" value="{{ $message }}">
        @endif
        <button type="submit" class="btn btn-danger float-right mr-2">Kembali</button>
      </form>
    </div>
  </div>
@section('js')
  <!-- Select2 -->
  <script src="{{ asset('/vendor/select2/js/select2.full.min.js') }}"></script>
  <!-- Page script -->
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('#select1').select2({
        theme: 'bootstrap4'
      })
      
      var i = 2;
      //Initialize Select2 Elements
      // $('.select2bs4').select2({
      //   theme: 'bootstrap4'
      // })

      $('#tambah-anggota').on('click', function(e){
        e.preventDefault();
        var teks = `<label for="" class="mt-1">ANGGOTA</label><select class="form-control select" id="select`+i+`" name="dosen[]" autofocus>
                    <option value="" hidden>--Pilih dosen</option>
                    @foreach ($dosen as $item)
                      <option value="{{$item->nidn}}"
                        @if (isset($penelitian))
                          @if ($item->nama == $penelitian->tahun_usulan)
                              selected
                          @endif
                        @endif
                      >{{$item->nama}}</option>
                    @endforeach
                  </select>`;
        $('#select-anggota').append(teks);
        $('#select'+i).select2({
          theme: 'bootstrap4'
        })
        i++;
      })      
    })
  </script>
@endsection