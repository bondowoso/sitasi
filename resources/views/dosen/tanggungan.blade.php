@extends('adminlte::page')

@section('usulan-open')
    menu-open
@endsection

@section('usulan-active')
    active
@endsection

@section('tanggungan-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tanggungan</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <p class="text-secondary">Daftar Tanggungan yang masih berlaku</p>
    <div class ="alert alert-success">
      Pemberitahuan! Tidak ada tanggungan  
    </div><br />
    <!-- /.alert -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
@endsection