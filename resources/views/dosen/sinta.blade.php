@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Informasi Detail Akun Sinta</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="nav-icon fas fa-user"></i> {{ $dosen->nama }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Sinta ID:</p>
                    <p>{{ $dosen->sinta_id }}</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Sinta Score:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Scopus H-Index:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Scopus Citations:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Scopus ID:</p>
                    <p>-</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Google H-Index:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Google Citations:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Google Articles:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Google i10:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Rank in National:</p>
                    <p>0</p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <p class="text-primary font-weight-bold mb-1">Rank in Affilation:</p>
                    <p>0</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
@endsection