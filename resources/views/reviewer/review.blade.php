@extends('adminlte::page')

@section('review-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Review</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Penelitian</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              @if ($message = Session::get('successpenelitian'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert"><i class="fas fa-times text-white"></i></button>
                  {{ $message }}
                </div>
              @endif
              @if ($penelitian)
                <table class="table" id="list-penelitian">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Anggota</th>
                      <th>Judul</th>
                      <th>Tahun Usulan</th>
                      <th>Tahun Pelaksana</th>
                      {{-- <td>Penelitian</td> --}}
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @isset($penelitian)
                      @foreach ($penelitian as $item)
                        <tr>
                          @if ($item['status']->status == 1)
                            <td><button class="btn btn-sm btn-success"> {{ $item['status']->step*20 }}% | Disetujui</button></td>
                          @elseif ($item['status']->status == 2)
                            <td><button class="btn btn-sm btn-danger"> {{ $item['status']->step*20 }}% | Ditolak</button></td>
                          @elseif ($item['status']->status == null)
                            <td><button class="btn btn-sm btn-secondary"> {{ $item['status']->step*20 }}% | Menunggu</button></td>
                          @endif
                          <td>
                            @if($item['anggota']->isEmpty())
                              <p class="text-center">{{ $item->nama }}</p>
                            @else
                              <ol>
                                <li>{{ $item->nama }}</li>
                                @foreach ($item['anggota'] as $anggota)
                                  <li>{{ $anggota->nama }}</li>
                                @endforeach
                              </ol>
                            @endif
                          </td>
                          <td>{{ $item->judul }}</td>
                          <td>{{ $item['skema']->nama }}</td>
                          <td>{{ $item->tahun_penelitian }}</td>
                          <td>
                            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal-files" data-value="{{ $item->id }}">Files</button>
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{ $item->id }}">Detail</button>
                          </td>
                        </tr>
                      @endforeach
                    @endisset
                  </tbody>
                </table>
              @else
                <!-- error template -->
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="error-template">
                        <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                        <h2 class="text-secondary">Tidak Ada Usulan</h2>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /error template -->
              @endif
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Pengabdian</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              @if ($message = Session::get('successpengabdian'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert"><i class="fas fa-times text-white"></i></button>
                  {{ $message }}
                </div>
              @endif
              @if ($pengabdian)
                <table class="table" id="list-pengabdian">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Anggota</th>
                      <th>Judul</th>
                      <th>Tahun Usulan</th>
                      <th>Tahun Pelaksana</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @isset($pengabdian)
                      @foreach ($pengabdian as $item)
                        <tr>
                          @if ($item['status']->status == 1)
                            <td><button class="btn btn-sm btn-success"> {{ $item['status']->step*20 }}% | Disetujui</button></td>
                          @elseif ($item['status']->status == 2)
                            <td><button class="btn btn-sm btn-danger"> {{ $item['status']->step*20 }}% | Ditolak</button></td>
                          @elseif ($item['status']->status == null)
                            <td><button class="btn btn-sm btn-secondary"> {{ $item['status']->step*20 }}% | Menunggu</button></td>
                          @endif
                          <td>
                            @if($item['anggota']->isEmpty())
                              <p class="text-center">{{ $item->nama }}</p>
                            @else
                              <ol>
                                <li>{{ $item->nama }}</li>
                                @foreach ($item['anggota'] as $anggota)
                                  <li>{{ $anggota->nama }}</li>
                                @endforeach
                              </ol>
                            @endif
                          </td>
                          <td>{{ $item->judul }}</td>
                          <td>{{ $item['skema']->nama }}</td>
                          <td>{{ $item->tahun_pengabdian }}</td>
                          <td>
                            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal-files-pengabdian" data-value="{{ $item->id }}">Files</button>
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-pengabdian" data-value="{{ $item->id }}">Detail</button>
                          </td>
                        </tr>
                      @endforeach
                    @endisset
                  </tbody>
                </table>
              @else
                <!-- error template -->
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="error-template">
                        <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                        <h2 class="text-secondary">Tidak Ada Usulan</h2>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /error template -->
              @endif
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Review</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="{{ route('reviewerReview.store') }}">
            @csrf
            <input type="hidden" name="id" id="id_penelitian" value="">
            <input type="hidden" name="kategori" id="kategori" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    Judul
                  </td>
                  <td id="modal-judul">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Abstrak
                  </td>
                  <td id="modal-abstrak">

                  </td>
                </tr>
                <tr>
                  <td>
                    Rumpun Ilmu
                  </td>
                  <td id="modal-rumpun_ilmu">

                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan
                  </td>
                  <td id="modal-usulan">

                  </td>
                </tr>
                <tr>
                <tr>
                  <td>
                    Progress
                  </td>
                  <td id="modal-progress">

                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan Dana
                  </td>
                  <td id="modal-usulan_dana">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Sumber Dana Lain
                  </td>
                  <td id="modal-sumber_dana_lain">
                    
                  </td>
                </tr>
                {{-- <tr>
                  <td>
                    Dana Disetujui
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr> --}}
                {{-- <tr>
                  <td>
                    Status File Laporan Akhir
                  </td>
                  <td id="#">
                    <select class="form-control" name="" required>
                      <option value="" hidden>--Pilih Status Kelengkapan</option>
                      <option value="">aPilih Dosen</option>
                    </select>
                  </td>
                </tr> --}}
                <tr>
                  <td>
                    Nilai
                  </td>
                  <td>
                    <input type="number" name="nilai" class="form-control" rows="4" name="komentar" placeholder="Nilai maks. 100" min="0" max="100">
                  </td>
                </tr>
                <tr>
                  <td>
                    Komentar
                  </td>
                  <td>
                    <textarea id="modal-komentar" class="form-control" rows="4" name="komentar" placeholder="Keterangan validasi" required>

                    </textarea>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-pengabdian">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Review</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="{{ route('reviewerReview.store') }}">
            @csrf
            <input type="hidden" name="id" id="id_pengabdian" value="">
            <input type="hidden" name="kategori" id="kategori2" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    Judul
                  </td>
                  <td id="modal-judul2">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Abstrak
                  </td>
                  <td id="modal-abstrak2">

                  </td>
                </tr>
                <tr>
                  <td>
                    Rumpun Ilmu
                  </td>
                  <td id="modal-rumpun_ilmu2">

                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan
                  </td>
                  <td id="modal-usulan2">

                  </td>
                </tr>
                <tr>
                <tr>
                  <td>
                    Progress
                  </td>
                  <td id="modal-progress2">

                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan Dana
                  </td>
                  <td id="modal-usulan_dana2">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Sumber Dana Lain
                  </td>
                  <td id="modal-sumber_dana_lain2">
                    
                  </td>
                </tr>
                {{-- <tr>
                  <td>
                    Dana Disetujui
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr> --}}
                {{-- <tr>
                  <td>
                    Status File Laporan Akhir
                  </td>
                  <td id="#">
                    <select class="form-control" name="" required>
                      <option value="" hidden>--Pilih Status Kelengkapan</option>
                      <option value="">aPilih Dosen</option>
                    </select>
                  </td>
                </tr> --}}
                <tr>
                  <td>
                    Nilai
                  </td>
                  <td>
                    <input type="number" name="nilai" class="form-control" rows="4" name="komentar" placeholder="Nilai maks. 100" min="0" max="100">
                  </td>
                </tr>
                <tr>
                  <td>
                    Komentar
                  </td>
                  <td>
                    <textarea id="modal-komentar2" class="form-control" rows="4" name="komentar" placeholder="Keterangan Validasi"></textarea>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-files">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Files</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td>
                  Proposal
                </td>
                <td id="file-proposal">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Kemajuan
                </td>
                <td id="laporan-kemajuan">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Akhir
                </td>
                <td id="laporan-akhir">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Anggaran
                </td>
                <td id="laporan-anggaran">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Belanja
                </td>
                <td id="laporan-belanja">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Publikasi
                </td>
                <td id="laporan-publikasi">
                  -
                </td>
              </tr>
              <tr>
                <td>
                Link Publikasi / Google Schoolar
                </td>
                <td id="link">
                  -
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->

    <div class="modal fade" id="modal-files">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Files</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td>
                  Proposal
                </td>
                <td id="file-proposal2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Kemajuan
                </td>
                <td id="laporan-kemajuan2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Akhir
                </td>
                <td id="laporan-akhir2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Anggaran
                </td>
                <td id="laporan-anggaran2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Belanja
                </td>
                <td id="laporan-belanja2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Publikasi
                </td>
                <td id="laporan-publikasi2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                Link Publikasi / Google Schoolar
                </td>
                <td id="link2">
                  -
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-files-pengabdian">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Files</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td>
                  Proposal
                </td>
                <td id="file-proposal2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Kemajuan
                </td>
                <td id="laporan-kemajuan2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Akhir
                </td>
                <td id="laporan-akhir2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Anggaran
                </td>
                <td id="laporan-anggaran2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Belanja
                </td>
                <td id="laporan-belanja2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                  Laporan Publikasi
                </td>
                <td id="laporan-publikasi2">
                  -
                </td>
              </tr>
              <tr>
                <td>
                Link Publikasi / Google Schoolar
                </td>
                <td id="link2">
                  -
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <!-- Page script -->
  <script type="text/javascript">
    $(document).ready( function () {
      //Datatables
      $('#list-penelitian').DataTable();
      $('#list-pengabdian').DataTable();

      $("#list-penelitian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/penelitian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#file-proposal').html('<a class="btn btn-success btn-sm" href="http://localhost:8000/'+d['berkas'][0].berkas+'">Unduh File</a>');
          $('#id_penelitian').val(d.id);
          $('#kategori').val('penelitian');
          $('#modal-judul').text(d.judul);
          $('#modal-abstrak').text(d.abstrak);
          $('#modal-usulan').text('Penelitian');
          $('#modal-rumpun_ilmu').text(d.rumpun_ilmu);
          $('#modal-progress').text(d['status'].step);
          $('#modal-komentar').val(d['berkas'][0].komentar);
          $('#modal-usulan_dana').text(d['dana'].usulan_dana);
          $('#modal-sumber_dana_lain').text(d['dana'].sumber_dana_lain);
        });
        console.log($(this).attr('data-value'));
      });

      $("#list-pengabdian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/pengabdian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#file-proposal2').html('<a class="btn btn-success btn-sm" href="'+d['berkas'][0].berkas+'">Unduh File</a>');
          $('#id_pengabdian').val(d.id);
          $('#kategori2').val('pengabdian');
          $('#modal-judul2').text(d.judul);
          $('#modal-abstrak2').text(d.abstrak);
          $('#modal-usulan2').text('Penelitian');
          $('#modal-rumpun_ilmu2').text(d.rumpun_ilmu);
          $('#modal-progress2').text(d['status'].step);
          $('#modal-komentar2').val(d['berkas'][0].komentar);
          $('#modal-usulan_dana2').text(d['dana'].usulan_dana);
          $('#modal-sumber_dana_lain2').text(d['dana'].sumber_dana_lain);
        });
        console.log($(this).attr('data-value'));
      });
  } );
  </script>
@endsection