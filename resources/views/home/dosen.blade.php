@extends('adminlte::page')

@section('general-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Dosen</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <!-- Widget: user widget style 1 -->
          <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-info">
              <h3 class="widget-user-username">{{ $dosen->nama }}</h3>
              <h5 class="widget-user-desc">{{ $dosen->nidn }}</h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle elevation-2" src="{{ asset('assets/img/1.jpg') }}" alt="User Avatar">
            </div>
            <div class="card-footer">
              <div class="card-body p-0">
                <table class="table text-nowrap">
                  <tbody>
                    <tr>
                      <td>Sinta</td>
                      <td>{{ $dosen->sinta_id }}</td>
                    </tr>
                    <tr>
                      <td>Google Scholar</td>
                      <td>{{ $dosen->google_scholar }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Skema yang bisa diajukan</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>Lembaga Penelitian dan Pengabdian Masyarakat</td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>Clean database</td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>Cron job running</td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>Fix and squish bugs</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Penelitian yang telah dilakukan</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Jenis</th>
                    <th>Abc</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>
                      Update software
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>
                      Clean database
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>
                      Cron job running
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>
                      Fix and squish bugs
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Pengabdian yang telah dilakukan</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Jenis</th>
                    <th>Abc</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>
                      Update software
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>
                      Clean database
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>
                      Cron job running
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>
                      Fix and squish bugs
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
@endsection