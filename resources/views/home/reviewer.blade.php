@extends('adminlte::page')

@section('general-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Reviewer</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>150</h3>

              <p>Jumlah Penelitian dan Pengabdian yang harus direview</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>53</h3>

              <p>Jumlah yang telah direview</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        {{-- <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>44</h3>

              <p>Jumlah yg belum direview</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col --> --}}
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Pengabdian yang belum direview</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-sm btn-success">Lihat lainnya</button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Jenis</th>
                    <th>Abc</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>
                      Update software
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>
                      Clean database
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>
                      Cron job running
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>
                      Fix and squish bugs
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Penelitian yang belum direview</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-sm btn-success">Lihat lainnya</button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Jenis</th>
                    <th>Abc</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>
                      Update software
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>
                      Clean database
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>
                      Cron job running
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>
                      Fix and squish bugs
                      <br> Absd askas asdjkas
                    </td>
                    <td>asdkjlasd</td>
                    <td>asdasldlkasdklasdas</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
@endsection