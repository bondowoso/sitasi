@extends('adminlte::page')

@section('reviewer-open')
    menu-open
@endsection

@section('reviewer-active')
    active
@endsection

@section('pembagian-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Pembagian Reviewer</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Data Usulan</h3>
              @if(session()->get('success'))
                <div class ="alert alert-success">
                  {{ session()->get('success') }}  
                </div><br />
              @endif
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table" id="list-pembagian">
                <thead>
                  <tr>
                    {{-- <th>No.</th> --}}
                    <th>Judul</th>
                    <th>Usulan</th>
                    <th>Tahun Pelaksana</th>
                    <th>Jenis Usulan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {{-- @foreach ($penelitian as $item) --}}
                    <tr>
                      {{-- <td>1</td> --}}
                      <td>Judul</td>
                      <td>2020</td>
                      <td>2020</td>
                      <td>Penelitian</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="#">Detail</button>
                      </td>
                    </tr>
                  {{-- @endforeach --}}
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
                <button type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah User</button>
                <a href="#">
                  <button type="button" class="btn btn-sm btn-success"><i class="fas fa-minus"></i> Generate Reviewer</button>
                </a>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Pilih Reviewer</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="#">
            @csrf
            <input type="hidden" name="id_dosen" id="id_dosen" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    Judul
                  </td>
                  <td id="modal-judul">
                    
                  </td>
                </tr>
                <tr>
                  <td style="width: 25%">
                    Reviewer I
                  </td>
                  <td id="">
                    <select class="form-control select2bs4" name="dosen[]" required>
                      <option value="" hidden>--Pilih Dosen</option>
                      {{-- @foreach ($dosen as $item)
                        <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                      @endforeach --}}
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Reviewer II
                  </td>
                  <td id="#">
                    <select class="form-control select2bs4" name="dosen[]" required>
                      <option value="" hidden>--Pilih Dosen</option>
                      {{-- @foreach ($dosen as $item)
                        <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                      @endforeach --}}
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <a href="#" class="btn btn-success"
                onclick="event.preventDefault();
                document.getElementById('acak').submit();">
                Pilih Acak
              </a>
              <form id="acak" action="#" method="POST" style="display: none;">
                @csrf
              </form>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <!-- Page script -->
  <script type="text/javascript">
    $(document).ready( function () {
      //Datatables
      $('#list-pembagian').DataTable();

      //Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      })

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      $( "#list-pembagian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-usulan/" + id, function( data ) {
          // console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-judul').text(d[0].judul);
        });
      });
  } );
  </script>
  <!-- Select2 -->
  <script src="{{ asset('/vendor/select2/js/select2.full.min.js') }}"></script>
@endsection