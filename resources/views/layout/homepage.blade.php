@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
@endsection
@section('content')
{{-- <div class="slide-one-item home-slider owl-carousel position-relative text-center">
    <div class="site-blocks-cover overlay h-75" style="background-image: url(assets/img/1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8">
                    <h1 class="text-white font-weight-light">SITASI</h1>
                    <h3 class="text-white font-weight-light mb-5">LPPM STMIK STIKOM Indonesia</h3>
                    {{-- <p class="mt-3 mb-5">SITASI merupakan sistem yang dapat melakukan pengajuan Penelitian, Pengabdian Dosen dan membantu proses yang terlibat didalamnya seperti progres pengajuan, mengelola data pengajuan, dll. sistem ini dibuat dengan tujuan meringankan pekerjaan ketua LPPM, mencegah hilangnya file karena virus atau bencana alam, dengan menggunakan tempat penyimpanan yang lebih terstruktur dan efektif seperti database, memperkecil kesalahan dalam penulisan nama dosen, dll.</p>
                    {{-- <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga est inventore ducimus repudiandae.</p>
                    <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Check Now!</a></p>
                </div>
            </div>
        </div>
    </div>  

    <div class="site-blocks-cover overlay" style="background-image: url(assets/img/1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8">
                    <h1 class="text-white font-weight-light">PENELITIAN</h1>
                    <h3 class="text-white font-weight-light mb-5">LPPM STMIK STIKOM Indonesia</h3>
                    <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Check Now!</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-blocks-cover overlay" style="background-image: url(assets/img/1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8">
                    <h1 class="text-white font-weight-light">PENGABDIAN</h1>
                    <h3 class="text-white font-weight-light mb-5">LPPM STMIK STIKOM Indonesia</h3>
                    <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Check Now!</a></p>
                </div>
            </div>
        </div>
    </div>  
</div> --}}

<div class="container">
    <div class="row mt-2">
        <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-3" style="margin-left: 0%">
            {{-- <div class="row"> --}}
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title text-center">Login</h3>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <input class="form-control" type="text" name="username" placeholder="Username" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">grafik satu</h5>
                            <canvas id="graf" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            {{-- </div> --}}
        </div>
        <div class="col-md-8 order-2 order-lg-1">
            <div class="card h-100">
                <div class="card-body">
                    <h3 class="card-title">Pengumuman</h3>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                Tanggal 
                                </th>
                                <th>
                                    Pengumuman
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ date('Y-m-d') }}</td>
                                <td>Pengumuman 1</td>
                            </tr>
                            <tr>
                                <td>{{ date('Y-m-d') }}</td>
                                <td>Pengumuman 2</td>
                            </tr>
                            <tr>
                                <td>{{ date('Y-m-d') }}</td>
                                <td>Pengumuman 3</td>
                            </tr>
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">pie susu satu</h5>
                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">pie susu dua</h5>
                    <canvas id="pie2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">Pie Susu Tiga</h5>
                    <canvas class="my-1" id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ url('/assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
{{-- <script src="{{ asset('assets/js/aos.js') }}"></script> --}}
<script>
    var siteCarousel = function () {
		if ( $('.nonloop-block-13').length > 0 ) {
			$('.nonloop-block-13').owlCarousel({
		    center: false,
		    items: 1,
		    loop: true,
				stagePadding: 0,
		    margin: 0,
		    autoplay: true,
		    nav: true,
				navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
		    responsive:{
	        600:{
	        	margin: 0,
	          items: 1
	        },
	        1000:{
	        	margin: 0,
	        	stagePadding: 0,
	          items: 1
	        },
	        1200:{
	        	margin: 0,
	        	stagePadding: 0,
	          items: 1
	        }
		    }
			});
		}

		$('.slide-one-item').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
	    margin: 0,
	    autoplay: true,
	    pauseOnHover: false,
	    nav: true,
	    navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
	  });
	};
	siteCarousel();
</script>
<script>
var graf = document.getElementById('graf').getContext('2d');
// var pie1 = document.getElementById('pie1').getContext('2d');
var pie2 = document.getElementById('pie2').getContext('2d');

var ctx = document.getElementById('graf');
var myChart = new Chart(graf, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'grafik',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(210, 214, 222, 1)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(210, 214, 222, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
// var myChart = new Chart(pie1, {
//     type: 'pie',
//     data: {
        
//         datasets: [{
//             label: 'Grafik 1',
//             data: [12, 19, 3],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     beginAtZero: true
//                 }
//             }]
//         }
//     }
// });
var myChart = new Chart(pie2, {
    type: 'pie',
    data: {
        
        datasets: [{
            label: 'Grafik 1',
            data: [12, 19],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
var donutData = {
    labels: [
        'Chrome', 
        'IE',
        'FireFox', 
        'Safari', 
        'Opera', 
        'Navigator', 
    ],
    datasets: [
    {
        data: [700,500,400,600,300,100],
        backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
    ]
}
var pieData        = donutData;
var pieOptions     = {
    maintainAspectRatio : false,
    responsive : true,
    legend: {
        position: 'bottom',
    },
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
    type: 'pie',
    data: pieData,
    options: pieOptions      
})

//-------------
//- BAR CHART -
//-------------
var areaChartData = {
    labels  : ['January 2020'],
    datasets: [
    {
        label               : 'Digital Goods',
        backgroundColor     : 'rgba(60,141,188,0.9)',
        borderColor         : 'rgba(60,141,188,0.9)',
        pointRadius         : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [50]
    },
    {
        label               : 'Electronics',
        backgroundColor     : 'rgba(210, 214, 222, 1)',
        borderColor         : 'rgba(210, 214, 222, 1)',
        pointRadius         : false,
        pointColor          : 'rgba(210, 214, 222, 1)',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(220,220,220,1)',
        data                : [48]
    }]
    // {
        // label               : 'Electronics',
        // backgroundColor     : 'rgba(210, 214, 222, 1)',
        // borderColor         : 'rgba(210, 214, 222, 1)',
        // pointRadius         : false,
        // pointColor          : 'rgba(210, 214, 222, 1)',
        // pointStrokeColor    : '#c1c7d1',
        // pointHighlightFill  : '#fff',
        // pointHighlightStroke: 'rgba(60,141,188,1)',
        // data                : [48]
    // },
    // ]
};
var barChartCanvas = $('#barChart').get(0).getContext('2d')
var barChartData = jQuery.extend(true, {}, areaChartData)
var temp0 = areaChartData.datasets[0]
var temp1 = areaChartData.datasets[1]
barChartData.datasets[0] = temp1
barChartData.datasets[1] = temp0

var barChartOptions = {
    responsive              : true,
    maintainAspectRatio     : false,
    datasetFill             : false,
    legend: {
        position : 'bottom'
    },
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
}

var barChart = new Chart(barChartCanvas, {
    type: 'bar', 
    data: barChartData,
    options: barChartOptions
})

</script>
@stop