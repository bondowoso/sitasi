<!DOCTYPE html>
<html>
<head>
	<title></title>
	@include('layout.meta')
	@yield('css')
</head>
<body>
	{{-- header --}}

	@include('layout.header')

	{{-- content --}}
	@yield('content')

	{{-- footer --}}
	@include('layout.footer')
	@yield('js')
</body>
</html>