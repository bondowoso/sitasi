@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="container">
    <p class="mt-4">
        SITASI merupakan sistem yang dapat melakukan pengajuan Penelitian, Pengabdian Dosen dan membantu proses yang terlibat didalamnya seperti progres pengajuan, mengelola data pengajuan, dll. sistem ini dibuat dengan tujuan meringankan pekerjaan ketua LPPM, mencegah hilangnya file karena virus atau bencana alam, dengan menggunakan tempat penyimpanan yang lebih terstruktur dan efektif seperti database, memperkecil kesalahan dalam penulisan nama dosen, dll. 
    <p> 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="myTable" class="display">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Author</th>
                                <th>Total Penelitian Internal</th>
                                <th>Total Penelitian Eksternal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4 col-md-2 text-center">
                                            <img class="img-responsive img-rounded pull-right" src="{{ asset('assets/img/user.png') }}" style="max-height: 100%; max-width: 100%;">
                                        </div>
                                        <div class="col-sm-8 col-md-10">
                                            0804018605
                                            <br>I Dewa Putu Gede Wiyata Putra, S.Kom., M.M.
                                            <br><button type="button" class="btn btn-success btn-xs">Lihat Detail Penelitian</button>
                                            <br><button type="button" class="btn btn-primary btn-xs">Lihat Google Scholar</button>
                                        </div>
                                    </div>
                                </td>
                                <td>2</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>
                                <div class="row">
                                    <div class="col-sm-4 col-md-2 text-center">
                                        <img class="img-responsive img-rounded pull-right" src="{{ asset('assets/img/user.png') }}" style="max-height: 100%; max-width: 100%;">
                                    </div>
                                    <div class="col-sm-8 col-md-10">
                                        0807038701
                                            <br>I Dewa Gede Agung Pandawana, S.Kom., M.Si
                                            <br><button type="button" class="btn btn-success btn-xs">Lihat Detail Penelitian</button>
                                            <br><button type="button" class="btn btn-primary btn-xs">Lihat Google Scholar</button>
                                        </div>
                                    </div>
                                </td>
                                <td>4</td>
                                <td>0</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ url('/assets/js/app.js') }}"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
// Datatables
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@stop