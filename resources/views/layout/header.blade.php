<nav class="navbar ">
	<div class="container">
		<a class="navbar-brand" href="{{ url("/") }}">SITASI</a>
		<ul class="nav justify-content-end">
			<li class="nav-item">
				<a class="nav-link active" href="{{ url("/") }}">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ url("/author") }}">Author</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Unduh</a>
			</li>
			{{-- <li class="nav-item">
				<a class="nav-link" href="#">Login</a>
			</li> --}}
		</ul>
	</div>
</nav>