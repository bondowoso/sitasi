<div class="modal fade" id="modal-tambahusulan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Skema Penelitian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('usulan.store') }}" method="post">
        @csrf
        <div class="modal-body">
          <input type="hidden" name="step" value="1">
          <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
          <div class="form-group">
            <label for="">NAMA SKEMA</label>
            <select class="form-control @error('skema') is-invalid @enderror tahun-skema" id="skema" name="skema" required autofocus>
              <option value="" hidden>--Pilih Tahun Usulan</option>
              @foreach ($skemaPenelitian as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>

            @error('tahun_usulan')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="">TAHUN PENELITIAN</label>
            <input class="form-control @error('tahun_penelitian') is-invalid @enderror" type="text" id="tahun_penelitian" name="tahun_penelitian" value="{{ date('Y') }}" required>

            @error('tahun_penelitian')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Selanjutnya</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-tambahpengabdian" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Skema Pengabdian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('pengabdian.store') }}" method="post">
        @csrf
        <div class="modal-body">
          <input type="hidden" name="step" value="1">
          <input type="hidden" name="id_dosen" value="{{ Auth::user()->username }}">
          <div class="form-group">
            <label for="">NAMA SKEMA</label>
            <select class="form-control @error('skema') is-invalid @enderror tahun-skema" id="skema" name="skema" required autofocus>
              <option value="" hidden>--Pilih Tahun Usulan</option>
              @foreach ($skemaPengabdian as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>

            @error('tahun_usulan')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="">TAHUN PENGABDIAN</label>
            <input class="form-control @error('tahun_pengabdian') is-invalid @enderror" type="text" id="tahun_pengabdian" name="tahun_pengabdian" value="{{ date('Y') }}" required>

            @error('tahun_pengabdian')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Selanjutnya</button>
        </div>
      </form>
    </div>
  </div>
</div>