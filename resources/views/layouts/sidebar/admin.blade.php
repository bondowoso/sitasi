<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/" class="nav-link @yield('general-active')">
        <i class="nav-icon fas fa-home"></i>
        <p>
          Beranda
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview @yield('datamaster-open')">
      <a href="#" class="nav-link @yield('datamaster-active')">
        <i class="nav-icon fas fa-database"></i>
        <p>
          Data Master
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('dosen.index') }}" class="nav-link @yield('dosen-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Dosen</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('user.index') }}" class="nav-link @yield('user-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>User</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('skema.index') }}" class="nav-link @yield('skema-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Skema</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link @yield('prodi-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Prodi</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview @yield('reviewer-open')">
      <a href="#" class="nav-link @yield('reviewer-active')">
        <i class="nav-icon fas fa-book"></i>
        <p>
          Review
          <i class="fas fa-angle-left right"></i>
        </p>
      </a> 
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('adminReviewer.index') }}" class="nav-link @yield('pembagian-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Pembagian Reviewer</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('adminPenilaian.index') }}" class="nav-link @yield('penilaian-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Penilaian</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview @yield('pemantauan-open')">
      <a href="#" class="nav-link @yield('pemantauan-active')">
        <i class="nav-icon fas fa-eye"></i>
        <p>
          Monitoring Internal
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{url('/admin/penelitian/usulan')}}" class="nav-link @yield('list-usulan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>List Usulan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Laporan Kemajuan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Laporan Akhir</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Laporan Belanja</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Publikasi</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview @yield('pemantauan-open')">
      <a href="#" class="nav-link @yield('datapendukung-active')">
        <i class="nav-icon fas fa-archive"></i>
        <p>
          Data Pendukung  
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Tambah Usulan</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('approver-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Approver
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('profil-active')">
        <i class="nav-icon fas fa-user"></i>
        <p>
          {{-- Profil Lembaga dan Pimpinan --}}
          Profil Lembaga dan...
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->