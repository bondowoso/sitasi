<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/" class="nav-link @yield('general-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('reviewerReview.index') }}" class="nav-link @yield('review-active')">
        <i class="nav-icon fas fa-book"></i>
        <p>
          Review
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('pelaksanaan-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Pelaksanaan
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('persetujuanpersonil-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Persetujuan Personil
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('verifikator-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Verifikator
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->