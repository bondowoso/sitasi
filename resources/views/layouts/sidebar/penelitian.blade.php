<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/" class="nav-link @yield('general-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview @yield('reviewer-open')">
      <a href="#" class="nav-link @yield('reviewer-active')">
        <i class="nav-icon fas fa-book"></i>
        <p>
          Review
          <i class="fas fa-angle-left right"></i>
        </p>
      </a> 
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link @yield('pembagian-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Pembagian Reviewer</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview @yield('usulan-open')">
      <a href="#" class="nav-link @yield('usulan-active')">
        <i class="nav-icon fas fa-copy"></i>
        <p>
          Usulan
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link @yield('tambahusulan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Tambah Usulan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link @yield('tanggungan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Tanggungan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link @yield('riwayatusulan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Riwayat Usulan</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('pelaksanaan-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Pelaksanaan
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('persetujuanpersonil-active')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Persetujuan Personil
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->