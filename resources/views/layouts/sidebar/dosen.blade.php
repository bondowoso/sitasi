@section('datamaster-active')
    active
@endsection

@section('dosen-active')
    
@endsection

<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/" class="nav-link @yield('general-active')">
        <i class="nav-icon fas fa-home"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview @yield('usulan-open')">
      <a href="#" class="nav-link @yield('usulan-active')">
        <i class="nav-icon fas fa-envelope"></i>
        <p>
          Penelitian
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link @yield('tambahusulan-active')" data-toggle="modal" data-target="#modal-tambahusulan" data-value="#">
            <i class="far fa-circle nav-icon"></i>
            <p>Tambah/Ubah Usulan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('usulan.index') }}" class="nav-link @yield('riwayatusulan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Riwayat Penelitian</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('tanggungan.index') }}" class="nav-link @yield('tanggungan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Tanggungan</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item has-treeview @yield('pengabdian-open')">
      <a href="#" class="nav-link @yield('pengabdian-active')">
        <i class="nav-icon fas fa-envelope"></i>
        <p>
          Pengabdian
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link @yield('tambahusulanpengabdian-active')" data-toggle="modal" data-target="#modal-tambahpengabdian" data-value="#">
            <i class="far fa-circle nav-icon"></i>
            <p>Tambah/Ubah Usulan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('tanggungan.index') }}" class="nav-link @yield('tanggungan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Tanggungan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('pengabdian.index') }}" class="nav-link @yield('riwayatusulan-active')">
            <i class="far fa-circle nav-icon"></i>
            <p>Riwayat Usulan</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('pelaksanaan-active')">
        <i class="nav-icon fas fa-calendar"></i>
        <p>
          Pelaksanaan
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('menupenelitian-active')">
        <i class="nav-icon fas fa-archive"></i>
        <p>
          Penelitian
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('menupengabdian-active')">
        <i class="nav-icon fas fa-archive"></i>
        <p>
          Pengabdian
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link @yield('persetujuanpersonil-active')">
        <i class="nav-icon fas fa-user"></i>
        <p>
          Persetujuan Personil
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->