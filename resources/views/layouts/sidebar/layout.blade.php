<!-- Sidebar -->
<div class="sidebar" style="padding-left: 0%; padding-right: 0%">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('assets/img/user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a class="d-block">{{ $username->nama }}</a>
            <a class="d-block">{{ $username->nidn }}</a>
        </div>
    </div>
    @include('layouts.sidebar.' . Auth::user()->getActiveRole(Auth::user()->id))
    </div>
<!-- /.sidebar -->

