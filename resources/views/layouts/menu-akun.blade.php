<div class="dropdown-menu dropdown-menu-right">
  <a class="dropdown-item" tabindex="-1" href="{{ route('profil.index') }}">Ubah Profil</a>                            
  @if (Auth::user()->hasRole("dosen"))
    <a class="dropdown-item" tabindex="-1" href="{{ route('dosen.sinta') }}">Akun Sinta</a>
  @endif
  <div class="dropdown-divider"></div>
  <a class="dropdown-item" tabindex="-1" href="#" 
      onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
  ><i class="fa fa-fw fa-power-off"></i> Log Out</a>
  <form id="logout-form" action="{{ $logout_url }}" method="POST" style="display: none;">
      @if(config('adminlte.logout_method'))
      {{ method_field(config('adminlte.logout_method')) }}
      @endif
      {{ csrf_field() }}
  </form>
</div>