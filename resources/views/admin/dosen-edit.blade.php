@extends('adminlte::page')

@section('datamaster-open')
    menu-open
@endsection

@section('datamaster-active')
    active
@endsection

@section('dosen-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Ubah Data Dosen</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Ubah Data Dosen</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('dosen.update', $dosen->id) }}">
              @csrf
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nidn">NIDN</label>
                      <input type="number" class="form-control" id="nidn" name="nidn" placeholder="Masukkan NIDN" value="{{ $dosen->nidn }}" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama" value="{{ $dosen->nama }}" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- textarea -->
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="4" name="alamat" placeholder="Masukkan Alamat">{{ $dosen->alamat }}</textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tempatlahir">Tempat Lahir</label>
                      <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="{{ $dosen->tempat_lahir }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukkan Tempat Lahir" value="{{ $dosen->tanggal_lahir }}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jabatan Fungsional</label>
                      <select class="form-control" id="jabatan_fungsional" name="jabatan_fungsional" required>
                        @foreach ($jabatan_akademik as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="ktp">No. KTP</label>
                      <input type="number" class="form-control" id="ktp" name="ktp" placeholder="Masukkan No. NIK KTP" value="{{ $dosen->ktp }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="telepon">No. Telepon</label>
                      <input type="number" class="form-control" id="telepon" name="telepon" placeholder="Masukkan No. Telepon" value="{{ $dosen->telepon }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="hp">No. HP</label>
                      <input type="number" class="form-control" id="hp" name="hp" placeholder="Masukkan No. HP" value="{{ $dosen->hp }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="{{ $dosen->email }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="website_personal">Alamat Website Pribadi</label>
                      <input type="text" class="form-control" id="website_personal" name="website_personal" placeholder="Masukkan Alamat Website Pribadi" value="{{ $dosen->website_personal }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="sinta_id">ID Sinta</label>
                      <input type="text" class="form-control" id="sinta_id" name="sinta_id" placeholder="Masukkan ID Sinta" value="{{ $dosen->sinta_id }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="google_scholar">ID Google Scholar</label>
                      <input type="text" class="form-control" id="google_scholar" name="google_scholar" placeholder="Masukkan ID Google Scholar" value="{{ $dosen->google_scholar }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="status" name="status" value="1" checked>
                      <label class="form-check-label font-weight-bold" for="status">Daftar Sebagai Dosen</label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
@endsection