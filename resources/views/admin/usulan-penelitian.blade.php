@extends('adminlte::page')

@section('pemantauan-open')
    menu-open
@endsection

@section('pemantauan-active')
    active
@endsection

@section('list-usulan-active')
    active
@endsection

@section('title', 'ADMIN-PAGE')

@section('content_header')
    <h1 class="m-0 text-dark">Master Data User</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Data User</h3>
              @if(session()->get('success'))
                <div class ="alert alert-success">
                  {{ session()->get('success') }}  
                </div><br />
              @endif
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="container">
                <table class="table" id="list-user">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Ketua</th>
                      <th>Judul</th>
                      <th>Skema</th>
                      <th>Usulan Tahun</th>
                      <th>Anggota</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $i)
                      <tr>
                        <td>{{$i['usulan']->step}}</td>
                        <td>{{$i['usulan']->nama}} - {{$i['usulan']->nidn}}</td>
                        <td>{{$i['usulan']->judul}}</td>
                        <td>{{ $i['usulan']->skema }}</td>
                        <td>{{$i['usulan']->tahun_penelitian}}</td>
                        <td>
                          <ul class="list-unstyled">
                            @foreach($i['anggota'] as $j)
                              <li>{{$j->nama}} - {{$j->nidn}}</li>
                            @endforeach
                          </ul>
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{$i['usulan']->id}}">Detail</button>
                         
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            
           
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    
  
  </section>
  <!-- /.content -->
@stop
