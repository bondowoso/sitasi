@extends('adminlte::page')

@section('datamaster-open')
    menu-open
@endsection

@section('datamaster-active')
    active
@endsection

@section('skema-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Master Data Skema</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Data Skema Penelitian</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="container">
                @if(session()->get('successpenelitian'))
                  <div class ="alert alert-success">
                    {{ session()->get('successpenelitian') }}  
                  </div><br />
                @endif
                <table class="table" id="table-penelitian">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($skemaPenelitian as $item)  
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>
                          <form action="{{ route('skemapenelitian.destroy', $item->id) }}" method="POST">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-skema" data-value="{{$item->id}}">Detail</button>
                            <a href="{{ route('skemapenelitian.edit', $item->id )}}" class="btn btn-sm btn-success">Ubah</a>
                            @csrf
                            @method('DELETE')
                              <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
                <a href="{{ route('skemapenelitian.create') }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah Skema</a>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Data Skema Pengabdian</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="container">
                @if(session()->get('successpengabdian'))
                  <div class="alert alert-success">
                    {{ session()->get('successpengabdian') }} 
                  </div><br />
                @endif
                <table class="table" id="table-pengabdian">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Skema Pengabdian</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($skemaPengabdian as $item)  
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>
                          <form action="{{ route('skemapengabdian.destroy', $item->id) }}" method="POST">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-skema" data-value="{{$item->id}}">Detail</button>
                            <a href="{{ route('skemapengabdian.edit', $item->id )}}" class="btn btn-sm btn-success">Ubah</a>
                            @csrf
                            @method('DELETE')
                              <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
                <a href="{{ route('skemapengabdian.create') }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah Skema</a>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>

  <!-- /.content -->
  <div class="modal fade" id="modal-skema">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detail Skema</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <table class="table">
          <tbody>
            <tr>
              <td>
                Nama Skema
              </td>
              <td id="modal-nama">
                
              </td>
            </tr>
            <tr>
              <td>
                Tahun Usulan
              </td>
              <td id="modal-usulan">
                
              </td>
            </tr>
            <tr>
              <td>
                Tahun Pelaksanaan
              </td>
              <td id="modal-pelaksanaan">
                
              </td>
            </tr>
            <tr>
              <td>
                Tanggal Daftar
              </td>
              <td id="modal-daftar">
                
              </td>
            </tr>
            <tr>
              <td>
                Tanggal Tutup
              </td>
              <td id="modal-tutup">
                
              </td>
            </tr>
            <tr>
              <td>
                Dana Maksimal
              </td>
              <td id="modal-dana">
                
              </td>
            </tr>
          </tbody>
        </table>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#table-penelitian').DataTable();
      $('#table-pengabdian').DataTable();
      $( "#table-penelitian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/skema/data-penelitian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-nama').text(d[0].nama);
          $('#modal-usulan').text(d[0].tahun_usulan);
          $('#modal-pelaksanaan').text(d[0].tahun_pelaksanaan);
          $('#modal-daftar').text(d[0].tanggal_daftar);
          $('#modal-tutup').text(d[0].tanggal_tutup);
          $('#modal-dana').text(d[0].dana_maksimal);
        });
      });
      $( "#table-pengabdian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/skema/data-pengabdian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-nama').text(d[0].nama);
          $('#modal-usulan').text(d[0].tahun_usulan);
          $('#modal-pelaksanaan').text(d[0].tahun_pelaksanaan);
          $('#modal-daftar').text(d[0].tanggal_daftar);
          $('#modal-tutup').text(d[0].tanggal_tutup);
          $('#modal-dana').text(d[0].dana_maksimal);
        });
      });
    })
  </script>
@endsection