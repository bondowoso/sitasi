@extends('adminlte::page')

@section('reviewer-open')
    menu-open
@endsection

@section('reviewer-active')
    active
@endsection

@section('pembagian-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Pembagian Reviewer</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">

        {{-- Penelitian --}}
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Usulan Penelitian</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if ($message = Session::get('successpenelitian'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert"><i class="fas fa-times text-white"></i></button>
                  {{ $message }}
                </div>
              @endif
              @if($penelitian)
                <table class="table" id="list-pembagian-penelitian">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Judul</th>
                      <th>Usulan</th>
                      <th>Tahun Pelaksana</th>
                      <th>Reviewer</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @isset($penelitian)
                      @foreach ($penelitian as $item)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $item->judul }}</td>
                          <td>{{ $item['skema']->nama }}</td>
                          <td>{{ $item->tahun_penelitian }}</td>
                          @isset($item->reviewer)  
                            <td>{{ $item['reviewer']->nama }}</td>
                          @else
                            <td>-</td>
                          @endisset
                          <td>
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{ $item->id }}">Detail</button>
                          </td>
                        </tr>
                      @endforeach
                    @endisset
                  </tbody>
                </table>
              @else
              <!-- error template -->
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="error-template">
                      <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                      <h2 class="text-secondary">Tidak Ada Usulan</h2>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /error template -->
              @endif
            </div>
            <!-- /.card-body -->
          @if ($penelitian)
            <div class="card-footer">
              @if (Auth::user()->hasRole("admin"))
                <a href="{{ route('adminReviewer.generatePenelitian') }}">
              @elseif (Auth::user()->hasRole("pimpinan"))
                <a href="{{ route('pimpinanReviewer.generatePenelitian') }}">
              @endif
                <button type="button" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> Generate Reviewer</button>
              </a>
            </div>
          @endif
          <!-- /.card -->
        </div>
        <!-- /.col -->

        {{-- Pengabdian --}}
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Usulan Pengabdian</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if ($message = Session::get('successpengabdian'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert"><i class="fas fa-times text-white"></i></button>
                  {{ $message }}
                </div>
              @endif
              @if($pengabdian)
                <table class="table" id="list-pembagian-pengabdian">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Judul</th>
                      <th>Usulan</th>
                      <th>Tahun Pelaksana</th>
                      <th>Reviewer</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @isset($pengabdian)
                      @foreach ($pengabdian as $item)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $item->judul }}</td>
                          <td>{{ $item['skema']->nama }}</td>
                          <td>{{ $item->tahun_pengabdian }}</td>
                          @isset($item->reviewer)  
                            <td>{{ $item['reviewer']->nama }}</td>
                          @else
                            <td>-</td>
                          @endisset
                          <td>
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-pengabdian" data-value="{{ $item->id }}">Detail</button>
                          </td>
                        </tr>
                      @endforeach
                    @endisset
                  </tbody>
                </table>
              @else
              <!-- error template -->
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="error-template">
                      <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                      <h2 class="text-secondary">Tidak Ada Usulan</h2>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /error template -->
              @endif
            </div>
            <!-- /.card-body -->
          @if ($pengabdian)
            <div class="card-footer">
              @if (Auth::user()->hasRole("admin"))
                <a href="{{ route('adminReviewer.generatePengabdian') }}">
              @elseif (Auth::user()->hasRole("pimpinan"))
                <a href="{{ route('pimpinanReviewer.generatePengabdian') }}">
              @endif
                <button type="button" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> Generate Reviewer</button>
              </a>
            </div>
          @endif
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <!-- Modal Penelitian -->
    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Pilih Reviewer</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {{-- @if (is_array($dosen) || is_object($dosen)) --}}
            @if (Auth::user()->hasRole("admin"))
              <form class="modal-body" method="POST" action="{{ route('adminReviewer.store') }}">
            @elseif (Auth::user()->hasRole("pimpinan"))
              <form class="modal-body" method="POST" action="{{ route('pimpinanReviewer.store') }}">
            @endif
              @csrf
              <input type="text" name="id" id="modal-id" value="" hidden>
              <input type="text" name="kategori" value="penelitian" hidden>
              <table class="table">
                <tbody>
                  <tr>
                    <td>
                      Judul
                    </td>
                    <td id="modal-judul">
                      
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 25%">
                      Reviewer
                    </td>
                    <td id="">
                      <select class="form-control select2bs4" name="reviewer1" required>
                        <option value="" hidden>--Pilih Dosen</option>
                        @foreach ($dosen as $item)
                          <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>
                  {{-- <tr>
                    <td>
                      Reviewer II
                    </td>
                    <td id="#">
                      <select class="form-control select2bs4" name="dosen[]" required>
                        <option value="" hidden>--Pilih Dosen</option>
                        @foreach ($dosen as $item)
                          <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr> --}}
                </tbody>
              </table>
              <div class="modal-footer justify-content-between">
                <a href="#" class="btn btn-success"
                  onclick="event.preventDefault();
                  document.getElementById('acak').submit();">
                  Pilih Acak
                </a>
                @if (Auth::user()->hasRole("admin"))
                  <form id="acak" action="{{ route('adminReviewer.generatePenelitian') }}" method="POST" style="display: none;">
                @elseif (Auth::user()->hasRole("pimpinan"))
                  <form id="acak" action="{{ route('pimpinanReviewer.generatePenelitian') }}" method="POST" style="display: none;">
                @endif
                  @csrf
                </form>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          {{-- @else
            <!-- error template -->
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="error-template">
                    <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                    <h2 class="text-secondary">Tidak Ada Reviewer</h2>
                  </div>
                </div>
              </div>
            </div>
            <!-- /error template -->
          @endif --}}
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Pengabdian -->
    <div class="modal fade" id="modal-pengabdian">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Pilih Reviewer</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {{-- @if (is_array($dosen) || is_object($dosen)) --}}
            @if (Auth::user()->hasRole("admin"))
              <form class="modal-body" method="POST" action="{{ route('adminReviewer.store') }}">
            @elseif (Auth::user()->hasRole("pimpinan"))
              <form class="modal-body" method="POST" action="{{ route('pimpinanReviewer.store') }}">
            @endif
              @csrf
              <input type="text" name="id" id="pengabdian-id" value="" hidden>
              <input type="text" name="kategori" value="pengabdian" hidden>
              <table class="table">
                <tbody>
                  <tr>
                    <td>
                      Judul
                    </td>
                    <td id="pengabdian-judul">
                      
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 25%">
                      Reviewer
                    </td>
                    <td id="">
                      <select class="form-control select2bs4" name="reviewer1" required>
                        <option value="" hidden>--Pilih Dosen</option>
                        @foreach ($dosen as $item)
                          <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>
                  {{-- <tr>
                    <td>
                      Reviewer II
                    </td>
                    <td id="#">
                      <select class="form-control select2bs4" name="dosen[]" required>
                        <option value="" hidden>--Pilih Dosen</option>
                        @foreach ($dosen as $item)
                          <option value="{{ $item->nidn }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr> --}}
                </tbody>
              </table>
              <div class="modal-footer justify-content-between">
                <a href="#" class="btn btn-success"
                  onclick="event.preventDefault();
                  document.getElementById('acak').submit();">
                  Pilih Acak
                </a>
                @if (Auth::user()->hasRole("admin"))
                  <form id="acak" action="{{ route('adminReviewer.generatePengabdian') }}" method="POST" style="display: none;">
                @elseif (Auth::user()->hasRole("pimpinan"))
                  <form id="acak" action="{{ route('pimpinanReviewer.generatePengabdian') }}" method="POST" style="display: none;">
                @endif
                  @csrf
                </form>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          {{-- @else
            <!-- error template -->
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="error-template">
                    <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                    <h2 class="text-secondary">Tidak Ada Reviewer</h2>
                  </div>
                </div>
              </div>
            </div>
            <!-- /error template -->
          @endif --}}
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <!-- Page script -->
  <script type="text/javascript">
    $(document).ready( function () {
      //Datatables
      $('#list-pembagian-penelitian').DataTable();
      $('#list-pembagian-pengabdian').DataTable();

      //Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      })

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      $( "#list-pembagian-penelitian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-usulan/penelitian/" + id, function( data ) {
          // console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-judul').text(d[0].judul);
          $('#modal-id').val(d[0].id);
        });
      });

      $( "#list-pembagian-pengabdian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-usulan/pengabdian/" + id, function( data ) {
          // console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#pengabdian-judul').text(d[0].judul);
          $('#pengabdian-id').val(d[0].id);
        });
      });
  } );
  </script>
  <!-- Select2 -->
  <script src="{{ asset('/vendor/select2/js/select2.full.min.js') }}"></script>
@endsection