@extends('adminlte::page')

@section('approver-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Approver</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Proposal</h3>
              @if(session()->get('success'))
                <div class ="alert alert-success">
                  {{ session()->get('success') }}  
                </div><br />
              @endif
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table" id="list-penilaian">
                <thead>
                  <tr>
                    {{-- <th>No.</th> --}}
                    <th>Tim Peneliti</th>
                    <th>Judul</th>
                    <th>Usulan</th>
                    <th>Pelaksana</th>
                    {{-- <th>Jenis Usulan</th> --}}
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {{-- @foreach ($penelitian as $item) --}}
                    <tr>
                      {{-- <td>1</td> --}}
                      <td>
                        <ul class="list-unstyled">
                          {{-- @foreach ($item['anggota'] as $anggota)
                            <li>{{ $anggota->nama }}</li>
                          @endforeach --}}
                          <li>Anggota 1 : I Made Dwi Putra Asana,S.Kom.,M.T.</li>
                        </ul>
                      </td>
                      <td>Judul</td>
                      <td>2020</td>
                      <td>2020</td>
                      {{-- <td>Penelitian</td> --}}
                      <td>
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="#">Detail</button>
                      </td>
                    </tr>
                  {{-- @endforeach --}}
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
                <button type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah User</button>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Penilaian Reviewer</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="#">
            @csrf
            <input type="hidden" name="id_dosen" id="id_dosen" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    Judul
                  </td>
                  <td id="modal-judul">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Abstrak
                  </td>
                  <td id="modal-abstrak">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Rumpun Ilmu
                  </td>
                  <td id="modal-rumpun-ilmu">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan
                  </td>
                  <td id="modal-usulan">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                <tr>
                  <td>
                    Pelaksana
                  </td>
                  <td id="modal-pelaksana">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Tim Peneliti
                  </td>
                  <td id="modal-tim-peneliti">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Progress
                  </td>
                  <td id="modal-progress">
                    <input type="text" class="form-control" name="" id="#" value="45 %  |  Menunggu" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan Dana
                  </td>
                  <td id="modal-usulan-dana">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Sumber Dana Lain
                  </td>
                  <td id="modal-sumber-dana-lain">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Score
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="">
                  </td>
                </tr>
                <tr>
                  <td>
                    Dana Disetujui
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Status File Laporan Akhir
                  </td>
                  <td id="#">
                    <select class="form-control" name="" required>
                      <option value="" hidden>--Pilih Status Kelengkapan</option>
                      <option value="">aPilih Dosen</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Keterangan Validasi
                  </td>
                  <td id="#">
                    <textarea class="form-control" rows="4" name="alamat" placeholder="Keterangan Validasi"></textarea>
                  </td>
                </tr>
                <tr>
                  <td>
                    Proposal
                  </td>
                  <td id="modal-proposal">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Kemajuan
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Akhir
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Anggaran
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Belanja
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Publikasi
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                  Link Publikasi / Google Schoolar
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <div class="row">
                <button type="button" class="btn btn-danger">Tidak Setuju</button>
                <button type="submit" class="btn btn-primary ml-1">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <!-- Page script -->
  <script type="text/javascript">
    $(document).ready( function () {
      //Datatables
      $('#list-penilaian').DataTable();

      $( "#list-penilaian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-penilaian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-judul').text(d[0].judul);
        });
        console.log($(this).attr('data-value'));
      });
  } );
  </script>
@endsection