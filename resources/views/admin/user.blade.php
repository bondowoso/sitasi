@extends('adminlte::page')

@section('datamaster-open')
    menu-open
@endsection

@section('datamaster-active')
    active
@endsection

@section('user-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Master Data User</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Data User</h3>
              @if(session()->get('success'))
                <div class ="alert alert-success">
                  {{ session()->get('success') }}  
                </div><br />
              @endif
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="container">
                <table class="table" id="list-user">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>NIDN</th>
                      <th>Nama</th>
                      <th>Role</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $i)
                      <tr>
                        <td>{{$i['user']->username}}</td>
                        <td>{{$i['user']->nidn}}</td>
                        <td>{{$i['user']->nama}}</td>
                        <td>
                          <ul>
                            @foreach($i['role'] as $j)
                              <li>{{$j->name}}</li>
                            @endforeach
                          </ul>
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{$i['user']->id}}">Detail</button>
                          <a href="{{ route('admin.reset_password', $i['user']->id) }}">
                            <button type="button" class="btn btn-sm btn-danger">Reset Password</button>
                          </a>
                          {{-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-password" data-value="{{$i['user']->id}}">Ubah Password</button> --}}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
                <button type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah User</button>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Detail User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="{{ route('admin.updateuser') }}">
            @csrf
            <input type="hidden" name="id_dosen" id="id_dosen" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    ID
                  </td>
                  <td id="modal-id">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Username
                  </td>
                  <td id="modal-username">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    NIDN
                  </td>
                  <td id="modal-nidn">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Nama
                  </td>
                  <td id="modal-nama">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Role
                  </td>
                  <td id="model-role">
                    <input type="checkbox" id="admin-cek" name="role[]" value="1" class="role-check">
                    <label for="admin"> Admin</label><br>
                    <input type="checkbox" id="pimpinan-cek" name="role[]" value="2" class="role-check">
                    <label for="pimpinan"> Pimpinan</label><br>
                    <input type="checkbox" id="dosen-cek" name="role[]" value="3" class="role-check">
                    <label for="dosen"> Dosen</label><br>
                    <input type="checkbox" id="penelitian-cek" name="role[]" value="4" class="role-check">
                    <label for="penelitian"> Penelitian</label><br>
                    <input type="checkbox" id="pengabdian-cek" name="role[]" value="5" class="role-check"> 
                    <label for="pengabdian"> Pengabdian</label><br>
                    <input type="checkbox" id="reviewer-cek" name="role[]" value="6" class="role-check"> 
                    <label for="reviewer"> Reviewer</label><br>
                    {{--<input type="checkbox" name="admin" value= "admin" checked>
                    <label for="admin"> Reviewer</label><br> --}}
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-password">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Ubah Password</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form class="modal-body" action="{{ route('admin.updatepassword') }}" role="form" method="post">
          @csrf
          @method('PATCH')
          <input type="hidden" name="id_dosen2" id="id_dosen2" value="">
          <div class="form-group">
            <label for="password">Ubah Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password Baru"required>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#list-user').DataTable();
      $( "#list-user tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/role-user/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#id_dosen').val(d[0].id);
          $('#id_dosen2').val(d[0].id);
          $('#modal-id').text(d[0].id);
          $('#modal-username').text(d[0].username);
          $('#modal-nidn').text(d[0].nidn);
          $('#modal-nama').text(d[0].nama);
          $('.role-check').each(function(){
            $(this).removeAttr("checked")
          });
         
          for (var i = 0; i < d[1].length; i++) {
            if ($('#admin-cek').val()==d[1][i].role_id) {
              $('#admin-cek').attr( "checked","checked" );
            }
           
            if ($('#dosen-cek').val()==d[1][i].role_id) {
              $('#dosen-cek').attr( "checked","checked" );
            }
      
            if ($('#pimpinan-cek').val()==d[1][i].role_id) {
              $('#pimpinan-cek').attr( "checked","checked" );
            }
        
            if ($('#penelitian-cek').val()==d[1][i].role_id) {
              $('#penelitian-cek').attr( "checked","checked" );
            }
        
            if ($('#pengabdian-cek').val()==d[1][i].role_id) {
              $('#pengabdian-cek').attr( "checked","checked" );
            }
        
            if ($('#reviewer-cek').val()==d[1][i].role_id) {
              $('#reviewer-cek').attr( "checked","checked" );
            }
            
          }
        });
        // console.log($(this).attr('data-value'));
      });
      
  } );
  </script>
@endsection