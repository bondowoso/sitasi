@extends('adminlte::page')

@section('datamaster-open')
    menu-open
@endsection

@section('datamaster-active')
    active
@endsection

@section('skema-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tambah Data {{ $judul }}</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Ubah Data {{ $judul }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route($link, $skema->id) }}">
              @csrf
              @method('PATCH')
              <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama">Nama {{ $judul }}</label>
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama {{ $judul }}" value="{{ $skema->nama }}">
                      @error('nama')
                        <br><div class="error">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="dana_maksimal">Dana Maksimal</label>
                      <input type="text" class="form-control" id="dana_maksimal" name="dana_maksimal" placeholder="Masukkan Dana Maksimal (Rp. )" value="{{ $skema->dana_maksimal }}">
                      @error('dana_maksimal')
                        <div class="error">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  @foreach ($form_tanggal as $item) 
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="{{ $item['value'] }}">{{ $item["nama"] }}</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="far fa-calendar-alt"></i>
                            </span>
                          </div>
                          <input type="date" class="form-control" id="{{ $item['value'] }}" name="{{ $item['value'] }}" placeholder="Masukkan {{ $item["nama"] }}" value="{{ $item['old'] }}">
                          @error('$item["value"]')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
@endsection