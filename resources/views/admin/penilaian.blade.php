@extends('adminlte::page')

@section('reviewer-open')
    menu-open
@endsection

@section('reviewer-active')
    active
@endsection

@section('penilaian-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Penilaian Reviewer</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          @if ($penelitian)
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Penelitian dan Pengabdian</h3>
                @if(session()->get('success'))
                  <div class ="alert alert-success">
                    {{ session()->get('success') }}  
                  </div><br />
                @endif
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table" id="list-penilaian">
                  <thead>
                    <tr>
                      {{-- <td>1</td> --}}
                      @if ($item['status']->status == 1)
                        <td><button class="btn btn-sm btn-success"> {{ $item['status']->total }} | Lengkap</button></td>
                      @else
                        <td><button class="btn btn-sm btn-secondary"> {{ $item['status']->total }} | Menunggu</button></td>
                      @endif
                      <td>
                        <ul class="list-unstyled">
                          <li>{{ $item->nama }}</li>
                          @foreach ($item['anggota'] as $anggota)
                            <li>{{ $anggota->nama }}</li>
                          @endforeach
                          {{-- <li>Anggota 1 : I Made Dwi Putra Asana,S.Kom.,M.T.</li> --}}
                        </ul>
                      </td>
                      <td>{{ $item->judul }}</td>
                      <td>{{ $item->tahun_usulan }}</td>
                      <td>{{ $item->tahun_penelitian }}</td>
                      {{-- <td>Penelitian</td> --}}
                      <td>
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{ $item->id }}">Detail</button>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($penelitian as $item)
                      <tr>
                        {{-- <td>1</td> --}}
                        @if ($item['status']->status == 1)
                          <td><button class="btn btn-sm btn-success"> {{ $item['status']->total }} | Lengkap</button></td>
                        @else
                          <td><button class="btn btn-sm btn-secondary"> {{ $item['status']->total }} | Menunggu</button></td>
                        @endif
                        <td>
                          <ul class="list-unstyled">
                            @foreach ($item['anggota'] as $anggota)
                              <li>{{ $anggota->nama }}</li>
                            @endforeach
                            {{-- <li>Anggota 1 : I Made Dwi Putra Asana,S.Kom.,M.T.</li> --}}
                          </ul>
                        </td>
                        <td>{{ $item->judul }}</td>
                        <td>{{ $item->tahun_usulan }}</td>
                        <td>{{ $item->tahun_penelitian }}</td>
                        {{-- <td>Penelitian</td> --}}
                        <td>
                          <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal-files" data-value="{{ $item->id }}">Files</button>
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{ $item->id }}">Detail</button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          @else
            <!-- error template -->
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="error-template">
                    <h1 class="text-secondary"><i class="fas fa-exclamation-circle"></i></h1>
                    <h2 class="text-secondary">Tidak Ada Penelitian/Pengabdian</h2>
                  </div>
                </div>
              </div>
            </div>
            <!-- /error template -->
          @endif
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Penilaian Reviewer</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="#">
            @csrf
            <input type="hidden" name="id_dosen" id="id_dosen" value="">
            <table class="table">
              {{-- <input type="text" name="id" id="modal-id"> --}}
              <tbody>
                <tr>
                  <td>
                    Judul
                  </td>
                  <td id="modal-judul">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Abstrak
                  </td>
                  <td id="modal-abstrak">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Rumpun Ilmu
                  </td>
                  <td id="modal-rumpun-ilmu">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan
                  </td>
                  <td id="modal-usulan">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                <tr>
                  <td>
                    Pelaksana
                  </td>
                  <td id="modal-pelaksana">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Tim Peneliti
                  </td>
                  <td id="modal-tim-peneliti">
                    <input type="text" class="form-control" name="" id="#" value="#" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Progress
                  </td>
                  <td id="modal-progress">
                    <input type="text" class="form-control" name="" id="#" value="45 %  |  Menunggu" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Usulan Dana
                  </td>
                  <td id="modal-usulan-dana">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Sumber Dana Lain
                  </td>
                  <td id="modal-sumber-dana-lain">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Score
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="">
                  </td>
                </tr>
                <tr>
                  <td>
                    Dana Disetujui
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="5000000" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Status File Laporan Akhir
                  </td>
                  <td id="#">
                    <select class="form-control" name="" required>
                      <option value="" hidden>--Pilih Status Kelengkapan</option>
                      <option value="">aPilih Dosen</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Keterangan Validasi
                  </td>
                  <td id="#">
                    <textarea class="form-control" rows="4" name="alamat" placeholder="Keterangan Validasi"></textarea>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-files">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Files</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="modal-body" method="POST" action="#">
            @csrf
            <table class="table">
              <tbody>
                <tr>
                  <td>
                    Proposal
                  </td>
                  <td id="modal-proposal">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Kemajuan
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Akhir
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Anggaran
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Belanja
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                    Laporan Publikasi
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
                <tr>
                  <td>
                  Link Publikasi / Google Schoolar
                  </td>
                  <td id="#">
                    <input type="text" class="form-control" name="" id="#" value="-" disabled>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <!-- Page script -->
  <script type="text/javascript">
    $(document).ready( function () {
      //Datatables
      $('#list-penilaian').DataTable();

      //Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      })

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      $( "#list-penilaian tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-penilaian/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-judul').text(d[0].judul);
      //     $('#id_dosen2').val(d[0].id);
      //     $('#modal-id').text(d[0].id);
      //     $('#modal-username').text(d[0].username);
      //     $('#modal-nidn').text(d[0].nidn);
      //     $('#modal-nama').text(d[0].nama);
      //     $('.role-check').each(function(){
      //       $(this).removeAttr("checked")
      //     });
         
      //     for (var i = 0; i < d[1].length; i++) {
      //       if ($('#admin-cek').val()==d[1][i].role_id) {
      //         $('#admin-cek').attr( "checked","checked" );
      //       }
           
      //       if ($('#dosen-cek').val()==d[1][i].role_id) {
      //         $('#dosen-cek').attr( "checked","checked" );
      //       }
      
      //       if ($('#pimpinan-cek').val()==d[1][i].role_id) {
      //         $('#pimpinan-cek').attr( "checked","checked" );
      //       }
        
      //       if ($('#penelitian-cek').val()==d[1][i].role_id) {
      //         $('#penelitian-cek').attr( "checked","checked" );
      //       }
        
      //       if ($('#pengabdian-cek').val()==d[1][i].role_id) {
      //         $('#pengabdian-cek').attr( "checked","checked" );
      //       }
            
      //     }
        });
        console.log($(this).attr('data-value'));
      });
      
  } );
  </script>
  <!-- Select2 -->
  <script src="{{ asset('/vendor/select2/js/select2.full.min.js') }}"></script>
@endsection