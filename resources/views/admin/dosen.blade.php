@extends('adminlte::page')

@section('datamaster-open')
    menu-open
@endsection

@section('datamaster-active')
    active
@endsection

@section('dosen-active')
    active
@endsection

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Master Data Dosen</h1>
@stop

@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/fontawesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@endsection

@section('content')
  <!-- Main content -->
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Data Dosen</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="container">
                @if(session()->get('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}  
                  </div><br />
                @endif
                <table class="table" id="list-dosen">
                  <thead>
                    <tr>
                      <th>NIDN</th>
                      <th>Nama</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($dosen as $i)
                      <tr>
                        <td>{{$i->nidn}}</td>
                        <td>{{$i->nama}}</td>
                        <td>
                          <form action="{{ route('dosen.destroy', $i->id) }}" method="POST">
                          <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default" data-value="{{$i->id}}">Detail</button>
                          <a href="{{ route('dosen.edit', $i->id) }}" class="btn btn-sm btn-primary">Ubah Data</a>
                          @csrf
                            <button type="submit" class="btn btn-sm btn-danger">Hapus Dosen</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <a href="{{ route('dosen.create') }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Tambah Dosen</a>
            </div>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Detail Dosen</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table">
              <tbody>
                <tr>
                  <td>
                    NIDN
                  </td>
                  <td id="modal-nidn">
                    
                  </td>
                </tr>
                <tr>
                  <td>
                    Nama
                  </td>
                  <td id="modal-nama">
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{-- <a href="{{ route('dosen.edit', $i->id) }}" class="btn btn-sm btn-primary">Ubah Data</button> --}}
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@stop

@section('js')
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#list-dosen').DataTable();
      $( "#list-dosen tbody tr td button" ).on( "click", function() {
        var id = $(this).attr('data-value');
        $.get( "/admin/data-dosen/" + id, function( data ) {
          console.log(JSON.parse(data));
          var d = JSON.parse(data);
          $('#modal-nidn').text(d[0].nidn);
          $('#modal-nama').text(d[0].nama);
        });
        // console.log($(this).attr('data-value'));
      });
      
  } );
  </script>
@endsection