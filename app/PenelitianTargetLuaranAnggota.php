<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenelitianTargetLuaranAnggota extends Model
{
    protected $table = "penelitian_target_luaran_anggota";

    protected $fillable = ['penulis_ke', 'tahun_publikasi'];
}
