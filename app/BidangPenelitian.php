<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidangPenelitian extends Model
{
    protected $table = "bidang_penelitian";

    protected $fillable = ['nama', 'aktif'];
}
