<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Role;
use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || 
                    abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) || 
                abort(401, 'This action is unauthorized.');
    }
    
    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public static function getActiveRole($id){
        $role_id = DB::table('role_user')->select('role_id')->where('user_id',$id)->first();
        $role = Role::find($role_id->role_id);
        return $role->name;
    }

    public static function userData($id)
    {
        $datauser = User::find($id);
        $user = $datauser->leftjoin('user_data', 'users.id', 'user_data.id')->where('users.id', $id)->first();

        return $user;
    }

    public static function userRole($id){
        $modal = new ListRole();
        $role = $modal->where('user_id', $id)->leftjoin('roles', 'role_id', 'roles.id')->get();

        return $role;
    }

    public static function roleData()
    {
        $modal = User::all();
        foreach ($modal as $key => $value) {
            $id = $value->id;
            $modaluser = User::find($id);
            $data[$key]['user'] = $modaluser->leftjoin('user_data', 'users.id', 'user_data.id')->where('users.id', $id)->first();
            $data[$key]['role'] = $modaluser->leftjoin('list_roles', 'users.id', 'list_roles.user_id')->leftjoin('roles', 'role_id', 'roles.id')->where('users.id', $id)->get();
        }

        return $data;
    }

    public static function storeUser($nidn)
    {
        $user = new User([
            'username' => $nidn,
            'password' => Hash::make($nidn)
        ]);
        $user->save();
        $user->roles()->attach(Role::where('name', 'dosen')->first());
    }

    public static function resetPassword($id_dosen)
    {
        $user = User::findOrFail($id_dosen);
        $user->password = Hash::make($user->username);
        $user->save();
    }

    public static function updatePassword($id_dosen, $password)
    {
        $user = User::findOrFail($id_dosen);
        $user->password = Hash::make($password);
        $user->save();
    }
}
