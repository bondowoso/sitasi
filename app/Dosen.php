<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = "user_data";

    protected $fillable = ['nidn', 'nama', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'ktp', 'telepon', 'hp', 'email', 'website_personal', 'jabatan_fungsional', 'sinta_id', 'google_scholar', 'status'];

    public static function getProfile($nidn)
    {
        $data = Dosen::firstWhere('nidn', $nidn);

        return $data;
    }

    public static function findId($id)
    {
        $result = Dosen::select('*')
                    ->where('user_data.id', $id)
                    ->leftJoin('role_user', 'user_data.id', 'role_user.user_id')
                    ->leftJoin('roles', 'role_user.role_id', 'roles.id')
                    ->first();

        return $result;
    }

    public static function storeDosen($nidn, $nama, $alamat, $tempat_lahir, $tanggal_lahir, $ktp, $telepon, $hp, $email, $website_personal, $jabatan_fungsional, $sinta_id, $google_scholar, $status)
    {
        $user_data = new Dosen([
            'nidn' => $nidn,
            'nama' => $nama,
            'alamat' => $alamat,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'ktp' => $ktp,
            'telepon' => $telepon,
            'hp' => $hp,
            'email' => $email,
            'website_personal' => $website_personal,
            'jabatan_fungsional' => $jabatan_fungsional,
            'sinta_id' => $sinta_id,
            'google_scholar' => $google_scholar,
            'status' => $status
        ]);
        $user_data->save();
    }

    public static function getKepalaLppm()
    {
        $data = Dosen::join('list_roles', 'user_data.id', '=' , 'list_roles.user_id')
                ->select('user_data.nidn', 'user_data.nama')
                ->orderByDesc('list_roles.created_at')
                ->firstWhere('list_roles.role_id', 2);
        
        return $data;
    }
}
