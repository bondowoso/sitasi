<?php

namespace App\Imports;

use App\Dosen;
use Maatwebsite\Excel\Concerns\ToModel;

class DosenImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Dosen([
            // 'name'     => $row[0],
            // 'email'    => $row[1],
            // 'password' => Hash::make($row[2]),
        ]);
    }
}
