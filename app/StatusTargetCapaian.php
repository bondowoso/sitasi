<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusTargetCapaian extends Model
{
    protected $table = "status_target_capaian";

    protected $fillable = ['nama', 'aktif'];
}
