<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Penelitian extends Model
{
    protected $table = "penelitian";

    protected $fillable = ['id_dosen', 'judul', 'skema', 'tahun_usulan', 'tahun_penelitian', 'abstrak', 'keyword', 'rumpun_ilmu', 'program_studi', 'kaprodi', 'kalppm', 'step', 'status', 'reviewer1', 'reviewer2'];
    
    protected $hidden = ['created_at', 'updated_at'];

    public static function getPenelitian()
    {
        $penelitian = Penelitian::all();
        if ($penelitian->isNotEmpty()) {
            foreach ($penelitian as $key => $value) {
                if ($value->step == 5) {
                    $id = $value->id;
        
                    $data[$key] = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                                                ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.reviewer1 as reviewer', 'penelitian.step')
                                                ->where('penelitian.step', 5)
                                                ->firstWhere('penelitian.id', $id);

                    $data[$key]['skema'] = Penelitian::join('skema_penelitian', 'penelitian.skema', 'skema_penelitian.id')
                                                        ->select('penelitian.skema as id', 'skema_penelitian.nama as nama')
                                                        ->firstWhere('skema_penelitian.id', $value->skema);
        
                    $data[$key]['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                                ->leftJoin('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                                ->where('penelitian_anggota.id_penelitian', $id)
                                                                ->get();

                    $data[$key]['reviewer'] = Penelitian::join('user_data', 'penelitian.reviewer1', 'user_data.nidn')
                                                            ->select('penelitian.reviewer1 as nidn', 'user_data.nama')
                                                            ->firstWhere('user_data.nidn', $value->reviewer1);
        
                    $data[$key]['status'] = PenelitianRiwayat::select('step', 'status')
                                                                ->where('id_penelitian', $id)
                                                                ->latest()
                                                                ->first();
        
                    $data[$key]['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                                ->where('id_penelitian', $id)
                                                                ->get();
        
                    $data[$key]['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                                            ->firstWhere('id_penelitian', $id);
                }
            }
    
            return $data;
        }
    }

    public static function getPenelitianByNidn($nidn)
    {
        $penelitian = Penelitian::where('id_dosen', $nidn)->get();
        if ($penelitian->isNotEmpty()) {
            foreach ($penelitian as $key => $value) {
                $id = $value->id;

                $data[$key] = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                                            ->join('skema_penelitian','skema_penelitian.id','penelitian.skema')
                                            ->leftJoin('penelitian_riwayat','penelitian_riwayat.id_penelitian','penelitian.id')
                                            ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.step', 'skema_penelitian.nama as skema', 'penelitian_riwayat.step as progress')
                                            ->firstWhere('penelitian.id', $id);

                $data[$key]['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                            ->leftJoin('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                            ->where('penelitian_anggota.id_penelitian', $id)
                                                            ->get();

                $data[$key]['status'] = PenelitianRiwayat::select('step', 'status')
                                                            ->where('id_penelitian', $id)
                                                            ->latest()
                                                            ->first();

                $data[$key]['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                            ->where('id_penelitian', $id)
                                                            ->get();

                $data[$key]['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                                        ->firstWhere('id_penelitian', $id);
            }
            
            return $data;
        }
    }

    public static function getPenelitianByReviewer($nidn)
    {
        $penelitian = Penelitian::where('reviewer1', $nidn)->get();
        if ($penelitian->isNotEmpty()) {
            foreach ($penelitian as $key => $value) {
                $id = $value->id;

                $data[$key] = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                                            ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.reviewer1 as reviewer', 'penelitian.step')
                                            ->firstWhere('penelitian.id', $id);

                $data[$key]['skema'] = Penelitian::join('skema_penelitian', 'penelitian.skema', 'skema_penelitian.id')
                                        ->select('penelitian.skema as id', 'skema_penelitian.nama as nama')
                                        ->firstWhere('skema_penelitian.id', $value->skema);

                $data[$key]['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                            ->leftJoin('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                            ->where('penelitian_anggota.id_penelitian', $id)
                                                            ->get();

                $data[$key]['reviewer'] = Penelitian::join('user_data', 'penelitian.reviewer1', 'user_data.nidn')
                                                        ->select('penelitian.reviewer1 as nidn', 'user_data.nama')
                                                        ->firstWhere('user_data.nidn', $value->reviewer1);

                $data[$key]['status'] = PenelitianRiwayat::select('step', 'status')
                                                            ->where('id_penelitian', $id)
                                                            ->latest()
                                                            ->first();

                $data[$key]['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                            ->where('id_penelitian', $id)
                                                            ->get();

                $data[$key]['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                                        ->firstWhere('id_penelitian', $id);
            }
            
            return $data;
        }
    }

    public static function firstPenelitian($id)
    {
        $data = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                            ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.step')
                            ->firstWhere('penelitian.id', $id);

        $data['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                ->leftJoin('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                ->where('penelitian_anggota.id_penelitian', $id)
                                                ->get();

        $data['status'] = PenelitianRiwayat::select('step', 'status')
                                                ->where('id_penelitian', $id)
                                                ->latest()
                                                ->first();

        $data['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_penelitian', $id)
                                                ->get();

        $data['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                        ->firstWhere('id_penelitian', $id);
        
        return $data;
    }

    public static function firstPenelitianByNidnWithSkema($nidn, $skema)
    {
        $data = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                            ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.step','penelitian.skema')
                            ->orderByDesc('penelitian.id')
                            ->where('skema', $skema)
                            ->firstWhere('penelitian.id_dosen', $nidn);

        if (isset($data)) {
            $data['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                    ->join('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                    ->where('penelitian_anggota.id_penelitian', $data->id)
                                                    ->get();
    
            $data['status'] = PenelitianRiwayat::select('step', 'status')
                                                ->where('id_penelitian', $data->id)
                                                ->latest()
                                                ->first();
    
            $data['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_penelitian', $data->id)
                                                ->get();
    
            $data['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                            ->firstWhere('id_penelitian', $data->id);
        }

        return $data;
    }

    public static function latestPenelitianByNidn($nidn)
    {
        $data = Penelitian::join('user_data', 'penelitian.id_dosen', 'user_data.nidn')
                            ->select('penelitian.id', 'penelitian.id_dosen as nidn', 'user_data.nama', 'penelitian.judul', 'penelitian.tahun_usulan', 'penelitian.tahun_penelitian', 'penelitian.abstrak', 'penelitian.keyword', 'penelitian.rumpun_ilmu', 'penelitian.program_studi', 'penelitian.kaprodi', 'penelitian.kalppm', 'penelitian.step','penelitian.skema')
                            ->orderByDesc('penelitian.id')
                            ->firstWhere('penelitian.id_dosen', $nidn);

        if (isset($data)) {
            $data['anggota'] = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                                                    ->join('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                                                    ->where('penelitian_anggota.id_penelitian', $data->id)
                                                    ->get();
    
            $data['status'] = PenelitianRiwayat::select('step', 'status')
                                                ->where('id_penelitian', $data->id)
                                                ->latest()
                                                ->first();
    
            $data['berkas'] = PenelitianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_penelitian', $data->id)
                                                ->get();
    
            $data['dana'] = PenelitianRab::select('usulan_dana', 'sumber_dana_lain')
                                            ->firstWhere('id_penelitian', $data->id);
        }

        return $data;
    }

    public static function countUsulan()
    {
        $data = Penelitian::count();

        return $data;
    }

    public static function getAllUsulan(){
        $usulan = DB::table('penelitian AS p')->select('p.id','judul','tahun_usulan','tahun_penelitian','pr.step','ud.nama','ud.nidn','sp.nama as skema')
                ->leftjoin('penelitian_riwayat AS pr','pr.id_penelitian','p.id')
                ->leftjoin('user_data AS ud','ud.nidn','p.id_dosen')
                ->leftjoin('skema_penelitian AS sp','sp.id','p.skema')
                ->get();
        $data = [];
        foreach ($usulan as $key => $u) {
            $anggota = PenelitianAnggota::select('user_data.nidn', 'user_data.nama')
                        ->join('user_data', 'penelitian_anggota.id_dosen', 'user_data.nidn')
                        ->where('penelitian_anggota.id_penelitian', $u->id)
                        ->get();
            
            $data[$key]['usulan'] = $u;
            $data[$key]['anggota'] = $anggota;
        }

        return $data;
    }


    public static function getOnGoingPenelitian($nidn,$tahun){
       
    }
}
