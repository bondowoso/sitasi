<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanPembiayaan extends Model
{
    protected $table = "status_pembiayaan";

    protected $fillable = ['nama', 'aktif'];
}
