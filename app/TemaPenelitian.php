<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemaPenelitian extends Model
{
    protected $table = "tema_penelitian";

    protected $fillable = ['nama', 'aktif'];
}
