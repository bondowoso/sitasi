<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopikPenelitian extends Model
{
    protected $table = "topik_penelitian";

    protected $fillable = ['nama', 'aktif'];
}
