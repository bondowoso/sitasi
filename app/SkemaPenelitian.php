<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SkemaPenelitian extends Model
{
    protected $table = "skema_penelitian";

    protected $fillable = ['nama', 'tanggal_usulan', 'tanggal_review', 'tanggal_laporan_kemajuan', 'tanggal_laporan_akhir', 'tanggal_publikasi', 'dana_maksimal','status'];

    public static function getSkemaByNIDN($nidn){

        DB::enableQueryLog();
        $data = DB::select(DB::raw("select `skema_penelitian`.`id`, `skema_penelitian`.`nama` from `skema_penelitian` WHERE (SELECT skema_penelitian.id from skema_penelitian LEFT JOIN penelitian ON skema_penelitian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5)  is null or (SELECT skema_penelitian.id from skema_penelitian LEFT JOIN penelitian ON skema_penelitian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5) <> skema_penelitian.id and `skema_penelitian`.`status` = 'aktif'"));
    	/*$data = SkemaPenelitian::select('skema_penelitian.id','skema_penelitian.nama')
        ->whereNull(DB::raw("(SELECT skema_penelitian.id from skema_penelitian LEFT JOIN penelitian ON skema_penelitian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5) "))
        ->orWhere(DB::raw("(SELECT skema_penelitian.id from skema_penelitian LEFT JOIN penelitian ON skema_penelitian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5)"),"<>","skema_penelitian.id")
            ->where('skema_penelitian.status',"'aktif'")
            ->get();*/
        $d = DB::getQueryLog();
        return $data;
    }
}
