<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengabdianRab extends Model
{
    protected $table = "pengabdian_rab";

    protected $fillable = ['id_pengabdian', 'usulan_dana', 'sumber_dana_lain'];
}
