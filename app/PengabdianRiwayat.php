<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengabdianRiwayat extends Model
{
    protected $table = "pengabdian_riwayat";

    protected $fillable = ['id_pengabdian', 'step', 'status', 'berkas', 'keterangan', 'nilai', 'komentar'];
}
