<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengabdianAnggota extends Model
{
    protected $table = "pengabdian_anggota";

    protected $fillable = ['id_pengabdian', 'id_dosen', 'status', 'persetujuan'];
}
