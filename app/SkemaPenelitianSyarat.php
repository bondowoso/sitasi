<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkemaPenelitianSyarat extends Model
{
    protected $table = "skema_penelitian_syarat";

    protected $fillable = ['nama', 'tahun_usulan'];
}
