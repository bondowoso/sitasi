<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $table = "program_studi";
    protected $fillable = ['nama', 'kepala', 'aktif'];
    protected $hidden = ['aktif', 'created_at', 'updated_at'];

    public static function getKepalaProdi()
    {
        return $data = ProgramStudi::where('aktif', 1)->get();
    }

    public static function firstKepalaProdi($kepalaProdiId)
    {
        return $data = ProgramStudi::firstWhere('id', $kepalaProdiId);
    }
}
