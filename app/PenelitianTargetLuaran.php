<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenelitianTargetLuaran extends Model
{
    protected $table = "penelitian_target_luaran";

    protected $fillable = ['status', 'tahun_capaian'];
}
