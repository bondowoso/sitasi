<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisLuaran extends Model
{
    protected $table = "jenis_luaran";

    protected $fillable = ['nama', 'aktif', 'status'];
}
