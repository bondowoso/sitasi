<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenReviewer extends Model
{
    protected $table = "dosen_reviewer";

    protected $fillable = ['nidn' ,'pen', 'peng'];
}
