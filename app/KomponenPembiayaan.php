<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomponenPembiayaan extends Model
{
    protected $table = "komponen_pembiayaan";

    protected $fillable = ['nama', 'aktif'];
}
