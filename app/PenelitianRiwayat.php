<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenelitianRiwayat extends Model
{
    protected $table = "penelitian_riwayat";

    protected $fillable = ['id_penelitian', 'step', 'status', 'berkas', 'keterangan', 'nilai', 'komentar'];
}
