<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RumpunIlmu extends Model
{
    protected $table = "rumpun_ilmu";

    protected $fillable = ['nama', 'aktif'];
}
