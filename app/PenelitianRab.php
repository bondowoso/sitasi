<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenelitianRab extends Model
{
    protected $table = "penelitian_rab";

    protected $fillable = ['id_penelitian', 'usulan_dana', 'sumber_dana_lain'];

    protected $hidden = ['id', 'created_at', 'updated_at'];
}
