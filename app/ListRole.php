<?php

namespace App;

use App\User;
use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;

class ListRole extends Model
{
    protected $table = "list_roles";

    protected $fillable = ['role_id', 'user_id'];

    public static function aaa()
    {
        $roles = ListRole::select('roles.description')
            ->join('roles', 'list_roles.role_id', 'roles.id')
            ->where('list_roles.user_id', Auth::user()->id)
            ->get();

        return $roles;
    }

    public static function storeListRole()
    {
        $user_id = User::orderBy('id', 'desc')->first();

        $role_list = new ListRole([
            'user_id' => $user_id->id,
            'role_id' => 3
        ]);
        $role_list->save();
    }

    public static function getReviewer()
    {
        DB::statement(DB::raw('SET @num=0'));
        $data = ListRole::selectRaw('@num:=@num+1 AS num')
                            ->selectRaw('user_data.nidn as nidn')
                            ->selectRaw('user_data.nama as nama')
                            ->leftJoin('user_data', 'list_roles.user_id', 'user_data.id')
                            ->where('list_roles.role_id', 6)
                            ->get();
        if (count($data) == 0) {
            return 0;
        } else {
            return $data;
        }    
    }

    public static function firstReviewer()
    {
        $data = ListRole::select('user_data.nidn as nidn', 'user_data.nama as nama')
                            ->leftJoin('user_data', 'list_roles.user_id', 'user_data.id')
                            ->where('list_roles.role_id', 6)
                            ->inRandomOrder()
                            ->first();
        
        return $data;
    }

    public static function countReviewer()
    {
        $data = ListRole::leftJoin('user_data', 'list_roles.user_id', 'user_data.id')
                            ->where('list_roles.role_id', 6)
                            ->count();
        
        return $data;
    }
}
