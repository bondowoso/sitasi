<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengabdianTargetLuaran extends Model
{
    protected $table = "pengabdian_target_luaran";

    protected $fillable = ['status', 'tahun_capaian'];
}
