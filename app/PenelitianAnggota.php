<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenelitianAnggota extends Model
{
    protected $table = "penelitian_anggota";

    protected $fillable = ['id_penelitian', 'id_dosen', 'status', 'persetujuan'];
}
