<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class SkemaPengabdian extends Model
{
    protected $table = "skema_pengabdian";

    protected $fillable = ['nama', 'tanggal_usulan', 'tanggal_review', 'tanggal_laporan_kemajuan', 'tanggal_laporan_akhir', 'tanggal_publikasi', 'dana_maksimal'];

    public static function getSkemaByNIDN($nidn)
    {
        DB::enableQueryLog();
        $data = DB::select(DB::raw("select `skema_pengabdian`.`id`, `skema_pengabdian`.`nama` from `skema_pengabdian` WHERE (SELECT skema_pengabdian.id from skema_pengabdian LEFT JOIN penelitian ON skema_pengabdian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5)  is null or (SELECT skema_pengabdian.id from skema_pengabdian LEFT JOIN penelitian ON skema_pengabdian.id = penelitian.skema WHERE penelitian.id_dosen = '".$nidn."' AND step >= 5) <> skema_pengabdian.id and `skema_pengabdian`.`status` = 'aktif'"));
        $d = DB::getQueryLog();
        return $data;
    }
}
