<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;

use App\ListRole;
use App\Penelitian;
use App\Pengabdian;
use App\SkemaPenelitian;
use App\SkemaPengabdian;
use App\ProgramStudi;
use App\RoleUser;
use App\User;


class HomeController extends Controller
{

	public function docTest(){
		return view('doctes');
	}
	public function docProses(Request $r){

		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($r->file);
		$head = $templateProcessor->cloneBlock('head');
		$body = $templateProcessor->cloneBlock('body');
		echo $head.'<br>';
		print_r($body);

	}
	public function index()
	{
		if (Auth::check()) {
			if (User::getActiveRole(Auth::user()->id) == 'admin') {
				return redirect('/admin');
			} elseif (User::getActiveRole(Auth::user()->id) == 'pimpinan') {
				return redirect('/pimpinan');
			} elseif (User::getActiveRole(Auth::user()->id) == 'dosen') {
				return redirect('/dosen');
			} elseif (User::getActiveRole(Auth::user()->id) == 'penelitian') {
				return redirect('/penelitian');
			} elseif (User::getActiveRole(Auth::user()->id) == 'pengabdian') {
				return redirect('/pengabdian');
			} elseif (User::getActiveRole(Auth::user()->id) == 'reviewer') {
				return redirect('/reviewer');
			}
		}
		return view('layout.homepage');
	}

	public function author()
	{
		return view('layout.author');
	}

	public function updateRole(Request $request)
	{
		$validatedData = $request->validate(['role_id' => 'required']);
		
		RoleUser::whereId(Auth::user()->id)->update($validatedData);

        return redirect('/');
	}

	public function getKepalaProdi()
	{
		return ProgramStudi::getKepalaProdi();
	}

	public function firstKepalaProdi($id)
	{
		return ProgramStudi::firstKepalaProdi($id);
	}

	public function firstPenelitian($id)
	{
        return json_encode(Penelitian::firstPenelitian($id));
	}

	public function firstPengabdian($id)
	{
        return json_encode(Pengabdian::firstPengabdian($id));
	}

	public function skemaPenelitian($id)
	{
		return json_encode(SkemaPenelitian::selectRaw('EXTRACT(YEAR FROM tanggal_usulan) AS tahun_usulan')->selectRaw('id')->whereId($id)->get());		
	}

	public function skemaPengabdian($id)
	{
		return json_encode(SkemaPengabdian::selectRaw('EXTRACT(YEAR FROM tanggal_usulan) AS tahun_usulan')->selectRaw('id')->whereId($id)->first());
	}
}
