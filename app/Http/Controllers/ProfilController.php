<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\JabatanAkademik;
use Auth;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::findOrFail(Auth::user()->id);
        $jabatan_akademik = JabatanAkademik::getJabatanAkademik(Auth::user()->id);

        // echo $jabatan_akademik;
        return view('dosen.profil', compact('dosen', 'jabatan_akademik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $dosen = Dosen::findOrFail(Auth::user()->id);
        $jabatan_akademik = JabatanAkademik::get();

        return view('dosen.profil-edit', compact('dosen', 'jabatan_akademik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nidn' => 'numeric',
            'alamat' => 'required',
            'ktp' => 'required|numeric',
            'telepon' => 'required|numeric',
            'hp' => 'required|numeric',
            'email' => 'required|email',
            'website_personal' => 'required',
            'jabatan_fungsional' => 'required'
        ]);

        Dosen::whereId($id)->update($validatedData);
        
        return redirect()->route('profil.index')->with('success', 'Profil berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
