<?php

namespace App\Http\Controllers;

use App\PengabdianAnggota;
use Illuminate\Http\Request;

class PengabdianAnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengabdianAnggota  $pengabdianAnggota
     * @return \Illuminate\Http\Response
     */
    public function show(PengabdianAnggota $pengabdianAnggota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengabdianAnggota  $pengabdianAnggota
     * @return \Illuminate\Http\Response
     */
    public function edit(PengabdianAnggota $pengabdianAnggota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengabdianAnggota  $pengabdianAnggota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengabdianAnggota $pengabdianAnggota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengabdianAnggota  $pengabdianAnggota
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengabdianAnggota $pengabdianAnggota)
    {
        //
    }
}
