<?php

namespace App\Http\Controllers\Penelitian;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ListRole;

class PenelitianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:penelitian');
    }

    public function index()
    {
        $roles = ListRole::where('user_id', Auth::user()->id);
 
        return view('home.penelitian', compact('roles'));
    }
}
