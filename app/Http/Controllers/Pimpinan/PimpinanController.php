<?php

namespace App\Http\Controllers\Pimpinan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ListRole;

class PimpinanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pimpinan');
    }

    public function index()
    {
        $roles = ListRole::where('user_id', Auth::user()->id);
 
        return view('home.pimpinan', compact('roles'));
    }
}
