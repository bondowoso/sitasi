<?php

namespace App\Http\Controllers\Reviewer;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

use App\Penelitian;
use App\PenelitianRiwayat;
use App\Pengabdian;
use App\PengabdianRiwayat;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penelitian = Penelitian::getPenelitianByReviewer(Auth::user()->username);
        $pengabdian = Pengabdian::getPengabdianByReviewer(Auth::user()->username);

        return view('reviewer.review', compact('penelitian', 'pengabdian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'kategori' => 'required',
            'nilai' => 'required',
            'komentar' => 'required'
        ]);

        if ($request->kategori == 'penelitian') {
            PenelitianRiwayat::where('id_penelitian', $request->id)->update(['nilai' => $request->nilai, 'komentar' => $request->komentar]);
            return back()->with('successpenelitian', 'Komentar usulan berhasil ditambahkan');
        } elseif ($request->kategori == 'pengabdian') {
            PengabdianRiwayat::where('id_pengabdian', $request->id)->update(['nilai' => $request->nilai, 'komentar' => $request->komentar]);
            return back()->with('successpengabdian', 'Komentar usulan berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Penelitian $penelitian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penelitian $penelitian)
    {
        //
    }
}
