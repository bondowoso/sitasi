<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Dosen;
use App\Penelitian;
use App\PenelitianAnggota;
use App\PenelitianRab;
use App\PenelitianRiwayat;
use App\ProgramStudi;
use App\SkemaPenelitian;
use App\RumpunIlmu;

class UsulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:dosen');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $penelitian = Penelitian::getPenelitianByNidn(Auth::user()->username);

        return view('dosen.usulan.index', compact('penelitian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $penelitian = Penelitian::firstPenelitianByNidnWithSkema(Auth::user()->username, $request->session()->get('skema'));
        if (isset($penelitian)) {
        } else {
            $penelitian = null;
        }
        
        $dosen = Dosen::where('nidn','<>',Auth::user()->username)->get();
        $kepala_lppm = Dosen::getKepalaLppm();
        $kepala_prodi = ProgramStudi::getKepalaProdi();
        $profil = Dosen::getProfile(Auth::user()->username);
        $program_studi = ProgramStudi::all();
        $rumpunIlmu = RumpunIlmu::all();

        //ambil skema yang belum selesai atau belum pernah dipilih
        // $skema = SkemaPenelitian::getSkemaByNIDN(Auth::user()->username);

        //$skema = SkemaPenelitian::all();
        //cek usulan
    
        $usulan = Penelitian::firstPenelitianByNidnWithSkema(Auth::user()->username, $request->session()->get('skema'));
        
        return view('dosen.usulan.form', compact('penelitian', 'dosen', 'kepala_lppm', 'kepala_prodi', 'profil', 'program_studi', 'rumpunIlmu', 'usulan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->step == 1) {
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' =>'required',
                'tahun_penelitian' => 'required'
            ]);
            //dd($validatedData);
            $penelitian = Penelitian::updateOrCreate(['id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
       
            return redirect()->route('usulan.create')->with('skema', $request->skema);
        }

        if ($request->step == 2) {
            //dd($request);
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'judul' => 'required',
                'abstrak' => 'required',
                'keyword' => 'required',
                'rumpun_ilmu' => 'required',
                'skema' => 'required',
            ]);
            
            if (empty($request->file('berkas'))) {
                $request->validate([
                    'id_penelitian => required',
                ]);

                $penelitian_riwayat = PenelitianRiwayat::updateOrCreate(['id_penelitian' => $request->id_penelitian, 'step' => 1], [
                    'id_penelitian' => $request->id_penelitian,
                    'step'          => 1,
                    'keterangan'    => "Proposal",
                ]);
            } else {
                $request->validate([
                    'id_penelitian => required',
                    'berkas' => 'required|mimes:pdf|max:1024'
                ]);

                $penelitian_riwayat = PenelitianRiwayat::updateOrCreate(['id_penelitian' => $request->id_penelitian, 'step' => 1], [
                    'id_penelitian' => $request->id_penelitian,
                    'step'          => 1,
                    'berkas'        => $request->file('berkas')->storeAs('proposal', (Auth::user()->username . " - " . $request->get('judul') . "." . $request->file('berkas')->getClientOriginalExtension())),
                    'keterangan'    => "Proposal",
                ]);
            }
            
            $penelitian = Penelitian::updateOrCreate(['step' => 1, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            
       
            return redirect()->route('usulan.create')->with('skema', $request->skema);
        }

        if ($request->step == 3) {
            if (isset($request->id_penelitian)) {
                PenelitianAnggota::where('id_penelitian', $request->id_penelitian)->delete();
            }
            
            $store_penelitian_anggota = new PenelitianAnggota();
            $penelitian_anggota = [];
            foreach ($request->dosen as $dosen) {
                if ($dosen != Auth::user()->username && $dosen != NULL) {
                    $data = [];
                    $data['id_penelitian'] = $request->id_penelitian;
                    $data['id_dosen'] = $dosen;
                    $data['status'] = 1;
                    $data['persetujuan'] = 1;
                    $penelitian_anggota[]=$data;
                }
            }
            $store_penelitian_anggota->insert($penelitian_anggota);

            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' => 'required',
            ]);
            $penelitian = Penelitian::updateOrCreate(['step' => 2, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            return redirect()->route('usulan.create')->with('skema', $request->skema);
        }

        if ($request->step == 4) {
            $penelitian_data = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' => 'required',
            ]);
            
            $penelitian_rab_data = $request->validate([
                'id_penelitian' => 'required',
                'usulan_dana' => 'required',
                'sumber_dana_lain' => 'required',
            ]);

            $penelitian = Penelitian::updateOrCreate(['step' => 3, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $penelitian_data);
            $penelitian_rab_data = PenelitianRab::updateOrCreate(['id_penelitian' => $request->id_penelitian], $penelitian_rab_data);
            return redirect()->route('usulan.create')->with('skema', $request->skema);
        }

        if ($request->step == 5) {
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'program_studi' => 'required',
                'kaprodi' => 'required',
                'kalppm' => 'required',
                'skema' => 'required',
            ]);

            $penelitian = Penelitian::updateOrCreate(['step' => 4, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            return redirect()->route('usulan.index')->with('success', 'Usulan berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penelitian = Penelitian::firstPenelitian($id);

        return json_encode($penelitian);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dosenId)
    {
        $penelitian = Penelitian::firstPenelitianByNidnWithSkema($dosenId, $request->skema);

        Penelitian::where('id_dosen', $dosenId)
                    ->where('skema', $request->skema)
                    ->update(['step' => $penelitian->step - 1]);

        return redirect()->route('usulan.create')->with('skema', $request->skema);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function editt(Request $request)
    {
        // return $request->skema;
        return redirect()->route('usulan.create')->with('skema', $request->skema);
    }
}
