<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Dosen;
use App\Pengabdian;
use App\PengabdianAnggota;
use App\PengabdianRab;
use App\PengabdianRiwayat;
use App\ProgramStudi;
use App\SkemaPengabdian;
use App\RumpunIlmu;
use Auth;
use Illuminate\Http\Request;

class UsulanPengabdianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:dosen');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengabdian = Pengabdian::getPengabdianByNidn(Auth::user()->username);

        return view('dosen.pengabdian.index', compact('pengabdian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $pengabdian = Pengabdian::firstPengabdianByNidnWithSkema(Auth::user()->username, $request->session()->get('skema'));
        if (isset($pengabdian)) {
        } else {
            $pengabdian = null;
        }
        
        $dosen = Dosen::where('nidn', '<>', Auth::user()->username)->get();
        $kepala_lppm = Dosen::getKepalaLppm();
        $kepala_prodi = ProgramStudi::getKepalaProdi();
        $profil = Dosen::getProfile(Auth::user()->username);
        $program_studi = ProgramStudi::all();
        $rumpunIlmu = RumpunIlmu::all();
        
        //ambil skema yang belum selesai atau belum pernah dipilih
        $skema = SkemaPengabdian::getSkemaByNIDN(Auth::user()->username);

        $usulan = Pengabdian::firstPengabdianByNidnWithSkema(Auth::user()->username, $request->session()->get('skema'));
        
        return view('dosen.pengabdian.form', compact('pengabdian', 'dosen', 'kepala_lppm', 'kepala_prodi', 'profil', 'program_studi', 'rumpunIlmu', 'skema', 'usulan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        if ($request->step == 1) {
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' => 'required',
                'tahun_pengabdian' => 'required'
            ]);

            $pengabdian = Pengabdian::updateOrCreate(['id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
       
            return redirect()->route('pengabdian.create')->with('skema', $request->skema);
        }

        if ($request->step == 2) {
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'judul' => 'required',
                'abstrak' => 'required',
                'keyword' => 'required',
                'rumpun_ilmu' => 'required',
                'nama_mitra' => 'required',
                'skema' => 'required',
            ]);
            
            if (empty($request->file('berkas'))) {
                $request->validate([
                    'id_pengabdian => required',
                ]);

                $pengabdian_riwayat = PengabdianRiwayat::updateOrCreate(['id_pengabdian' => $request->id_pengabdian, 'step' => 1], [
                    'id_pengabdian' => $request->id_pengabdian,
                    'step'          => 1,
                    'keterangan'    => "Proposal",
                ]);
            } else {
                $request->validate([
                    'id_pengabdian => required',
                    'berkas' => 'required|mimes:pdf|max:1024'
                ]);

                $pengabdian_riwayat = PengabdianRiwayat::updateOrCreate(['id_pengabdian' => $request->id_pengabdian, 'step' => 1], [
                    'id_pengabdian' => $request->id_pengabdian,
                    'step'          => 1,
                    'berkas'        => $request->file('berkas')->storeAs('proposal', (Auth::user()->username . " - " . $request->get('judul') . "." . $request->file('berkas')->getClientOriginalExtension())),
                    'keterangan'    => "Proposal",
                ]);
            }
            
            $pengabdian = Pengabdian::updateOrCreate(['step' => 1, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            
            return redirect()->route('pengabdian.create')->with('skema', $request->skema);
        }

        if ($request->step == 3) {
            if (isset($request->id_pengabdian)) {
                PengabdianAnggota::where('id_pengabdian', $request->id_pengabdian)->delete();
            }
            
            $store_pengabdian_anggota = new PengabdianAnggota();
            $pengabdian_anggota = [];
            foreach ($request->dosen as $dosen) {
                if ($dosen != Auth::user()->username && $dosen != NULL) {
                    $data = [];
                    $data['id_pengabdian'] = $request->id_pengabdian;
                    $data['id_dosen'] = $dosen;
                    $data['status'] = 1;
                    $data['persetujuan'] = 1;
                    $pengabdian_anggota[]=$data;
                }
            }
            $store_pengabdian_anggota->insert($pengabdian_anggota);

            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' => 'required',
            ]);
            $pengabdian = Pengabdian::updateOrCreate(['step' => 2, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            return redirect()->route('pengabdian.create')->with('skema', $request->skema);
        }

        if ($request->step == 4) {
            $pengabdian_data = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'skema' => 'required',
            ]);
            
            $pengabdian_rab_data = $request->validate([
                'id_pengabdian' => 'required',
                'usulan_dana' => 'required',
                'sumber_dana_lain' => 'required',
            ]);

            $pengabdian = Pengabdian::updateOrCreate(['step' => 3, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $pengabdian_data);
            $pengabdian_rab_data = PengabdianRab::updateOrCreate(['id_pengabdian' => $request->id_pengabdian], $pengabdian_rab_data);
            return redirect()->route('pengabdian.create')->with('skema', $request->skema);
        }

        if ($request->step == 5) {
            $validatedData = $request->validate([
                'step' => 'required',
                'id_dosen' => 'required',
                'program_studi' => 'required',
                'kaprodi' => 'required',
                'kalppm' => 'required',
                'skema' => 'required',
            ]);

            $pengabdian = Pengabdian::updateOrCreate(['step' => 4, 'id_dosen' => Auth::user()->username, 'status' => NULL, 'skema' => $request->skema], $validatedData);
            return redirect()->route('pengabdian.index')->with('success', 'Usulan berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengabdian = Pengabdian::firstPengabdian($id);

        return json_encode($pengabdian);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengabdian  $pengabdian
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengabdian $pengabdian)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pengabdian  $pengabdian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dosenId)
    {
        $pengabdian = Pengabdian::firstPengabdianByNidnWithSkema($dosenId, $request->skema);
        
        Pengabdian::where('id_dosen', $dosenId)
                    ->where('skema', $request->skema)
                    ->update(['step' => $pengabdian->step - 1]);

        return redirect()->route('pengabdian.create')->with('skema', $request->skema);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pengabdian  $pengabdian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengabdian $pengabdian)
    {
        //
    }
}
