<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ListRole;
use App\Dosen;

class DosenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:dosen');
    }

    public function index()
    {
        $dosen = Dosen::findOrFail(Auth::user()->id);
        return view('home.dosen', compact('dosen'));
    }
    
    public function sinta()
    {
        $dosen = Dosen::findOrFail(Auth::user()->id);
        return view('dosen.sinta', compact('dosen'));
    }
}
