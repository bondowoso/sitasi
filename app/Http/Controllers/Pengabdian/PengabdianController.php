<?php

namespace App\Http\Controllers\Pengabdian;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ListRole;

class PengabdianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengabdian');
    }

    public function index()
    {
        $roles = ListRole::where('user_id', Auth::user()->id);
 
        return view('home.pengabdian', compact('roles'));
    }
}
