<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

use App\ListRole;
use App\Penelitian;
use App\Pengabdian;

class ReviewerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|pimpinan');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penelitian = Penelitian::getPenelitian();
        $pengabdian = Pengabdian::getPengabdian();
        $dosen = ListRole::getReviewer();

        return view('admin.pembagian-reviewer', compact('penelitian', 'pengabdian', 'dosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'kategori' => 'required',
            'reviewer1' => 'required'
        ]);

        if ($request->kategori == 'penelitian') {
            Penelitian::where('id', $request->id)->update(['reviewer1' => $request->reviewer1]);
            return back()->with('successpenelitian', 'Reviewer berhasil diubah');
        } else if ($request->kategori == 'pengabdian') {
            Pengabdian::where('id', $request->id)->update(['reviewer1' => $request->reviewer1]);
            return back()->with('successpengabdian', 'Reviewer berhasil diubah');
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function show(Penelitian $penelitian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function edit(Penelitian $penelitian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penelitian $penelitian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penelitian  $penelitian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penelitian $penelitian)
    {
        //
    }

    function generateReviewerPenelitian()
    {
        $usulan = Penelitian::all();
        
        foreach ($usulan as $item) {
            do {
                $dosen = ListRole::firstReviewer();
            } while ($dosen->nidn == $item->id_dosen);

            Penelitian::whereId($item->id)
                        ->where('reviewer1', NULL)
                        ->update(['reviewer1' => $dosen->nidn]);
        }
        
        return redirect()->route('adminReviewer.index')->with('success', 'Reviewer berhasil ditambah');
    }

    function generateReviewerPengabdian()
    {
        $usulan = Pengabdian::all();
        
        foreach ($usulan as $item) {
            do {
                $dosen = ListRole::firstReviewer();
            } while ($dosen->nidn == $item->id_dosen);

            Pengabdian::whereId($item->id)
                        ->where('reviewer1', NULL)
                        ->update(['reviewer1' => $dosen->nidn]);
        }
        
        return redirect()->route('adminReviewer.index')->with('success', 'Reviewer berhasil ditambah');
    }
}
