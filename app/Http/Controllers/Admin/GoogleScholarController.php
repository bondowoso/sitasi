<?php

namespace App\Http\Controllers\Admin;

use Goutte\Client;
use GScholarProfileParser\DomCrawler\ProfilePageCrawler;
use GScholarProfileParser\Entity\Publication;
use GScholarProfileParser\Entity\Statistics;
use GScholarProfileParser\Iterator\PublicationYearFilterIterator;
use GScholarProfileParser\Parser\PublicationParser;
use GScholarProfileParser\Parser\StatisticsParser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleScholarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    function statistics()
    {
        /** @var Client $client */
        $client = new Client();
    
        /** @var ProfilePageCrawler $crawler */
        $crawler = new ProfilePageCrawler($client, 'b1wUPgMAAAAJ&hl'); // the second parameter is the scholar's profile id
    
        /** @var StatisticsParser $parser */
        $parser = new StatisticsParser($crawler->getCrawler());
    
        /** @var Statistics $statistics */
        $statistics = new Statistics($parser->parse());
    
        $nbCitationsPerYear = $statistics->getNbCitationsPerYear();
        $sinceYear = $statistics->getSinceYear();
    
        $nbCitationsSinceYear = 0;
        foreach ($nbCitationsPerYear as $year => $nbCitations) {
            if ($year >= $sinceYear) {
                $nbCitationsSinceYear += $nbCitations;
            }
        }
    
        // display statistics
        echo sprintf("           All\t%4d<br>", $sinceYear);
        echo sprintf("Citations: %4d\t%4d<br>", $statistics->getNbCitations(), $nbCitationsSinceYear);
        echo sprintf("h-index  : %4d\t%4d<br>", $statistics->getHIndex(), $statistics->getHIndexSince());
        echo sprintf("i10-index: %4d\t%4d<br>", $statistics->getI10Index(), $statistics->getI10IndexSince());
        echo "<br>";
        echo implode("\t", array_keys($nbCitationsPerYear));
        echo "<br>";
        echo implode("\t", array_values($nbCitationsPerYear));
        echo "<br>";
    }

    // function publications()
    // {
    //     /** @var Client $client */
    //     $client = new Client();

    //     /** @var ProfilePageCrawler $crawler */
    //     $crawler = new ProfilePageCrawler($client, 'b1wUPgMAAAAJ&hl'); // the second parameter is the scholar's profile id

    //     /** @var PublicationParser $parser */
    //     $parser = new PublicationParser($crawler->getCrawler());

    //     /** @var array<int, array<string, string>> $publications */
    //     $publications = $parser->parse();

    //     // hydrates items of $publications into Publication
    //     foreach ($publications as $publication) {
    //         /** @var Publication $publication */
    //         $publication = new Publication($publication);
    //     }
    //     unset($publication);

    //     /** @var Publication $latestPublication */
    //     // dd($publications);
    //     // $latestPublication = $publications[0];
    //     // var_dump($latestPublication);
    //     // displays latest publication data
    //     // echo $latestPublication['title'], "\n";
    //     // echo $latestPublication['publicationPath'], "\n";
    //     // echo $latestPublication['authors'], "\n";
    //     // echo $latestPublication['publisherDetails'], "\n";
    //     // echo $latestPublication['year'], "\n";
    //     // echo $latestPublication->getPublicationURL(), "\n";
    //     // echo $latestPublication->getAuthors(), "\n";
    //     // echo $latestPublication->getPublisherDetails(), "\n";
    //     // echo $latestPublication->getNbCitations(), "\n";
    //     // echo $latestPublication->getCitationsURL(), "\n";
    //     // echo $latestPublication->getYear(), "\n";

    //     /** @var PublicationYearFilterIterator $publications2018 */
    //     // $publications2018 = new PublicationYearFilterIterator(new ArrayIterator($publications), 2019);

    //     // displays list of publications published in 2018
    //     /** @var Publication $publication */
    //     return json_encode($publications);
    //     // foreach ($publications as $publication) {
    //     //     echo $publication['title'], "<br>";
    //     //     echo $publication['publicationPath'], "<br>";
    //     //     echo $publication['authors'], "<br>";
    //     //     echo $publication['publisherDetails'], "<br>";
    //     //     echo $publication['year'], "<br>";
    //     // }
    // }
    
    
    function publications()
    {
        /** @var Client $client */
        $client = new Client();

        /** @var ProfilePageCrawler $crawler */
        $crawler = new ProfilePageCrawler($client, 'b1wUPgMAAAAJ&hl'); // the second parameter is the scholar's profile id

        /** @var PublicationParser $parser */
        $parser = new PublicationParser($crawler->getCrawler());

        /** @var array<int, array<string, string>> $publications */
        $publications = $parser->parse();

        // hydrates items of $publications into Publication
        foreach ($publications as $publication) {
            /** @var Publication $publication */
            $publication = new Publication($publication);
        }
        unset($publication);
        
        // displays list of publications
        /** @var Publication $publication */
        return json_encode($publications);
    }

    function publication()
    {
        /** @var Client $client */
        $client = new Client();

        /** @var ProfilePageCrawler $crawler */
        $crawler = new ProfilePageCrawler($client, 'b1wUPgMAAAAJ&hl'); // the second parameter is the scholar's profile id

        /** @var PublicationParser $parser */
        $parser = new PublicationParser($crawler->getCrawler());

        /** @var array<int, array<string, string>> $publications */
        $publications = $parser->parse();

        // hydrates items of $publications into Publication
        foreach ($publications as $publication) {
            /** @var Publication $publication */
            $publication = new Publication($publication);
        }
        unset($publication);

        /** @var Publication $publications[0] */
        return json_encode($publications[0]);
    }
}