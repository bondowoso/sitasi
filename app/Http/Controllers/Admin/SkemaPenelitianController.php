<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SkemaPenelitian;
use Illuminate\Http\Request;

class SkemaPenelitianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $judul = "Skema Penelitian";
        $link = "skemapenelitian.store";
        $form_tanggal = [
            [
                'nama' => 'Tanggal Usulan',
                'value' => 'tanggal_usulan'
            ],
            [
                'nama' => 'Tanggal Review',
                'value' => 'tanggal_review'
            ],
            [
                'nama' => 'Tanggal Laporan Kemajuan',
                'value' => 'tanggal_laporan_kemajuan'
            ],
            [
                'nama' => 'Tanggal Laporan Akhir',
                'value' => 'tanggal_laporan_akhir'
            ],
            [
                'nama' => 'Tanggal Publikasi',
                'value' => 'tanggal_publikasi'
            ]
        ];
        
        return view('admin.skema-create', compact('judul', 'link', 'form_tanggal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:75',
            'tanggal_usulan' => 'required|date',
            'tanggal_review' => 'required|date',
            'tanggal_laporan_kemajuan' => 'required|date',
            'tanggal_laporan_akhir' => 'required|date',
            'tanggal_publikasi' => 'required|date',
            'dana_maksimal' => 'required|numeric',
            'status' => 'required'
        ]);

        $skemaPenelitian = SkemaPenelitian::create($validatedData);
   
        return redirect()->route('skema.index')->with('successpenelitian', 'Skema penelitian berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SkemaPenelitian  $skemaPenelitian
     * @return \Illuminate\Http\Response
     */
    public function show(SkemaPenelitian $skemaPenelitian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SkemaPenelitian  $skemaPenelitian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $judul = "Skema Penelitian";
        $link = "skemapenelitian.update";
        $skema = SkemaPenelitian::where('id', $id)->first();
        $form_tanggal = [
            [
                'nama' => 'Tanggal Usulan',
                'value' => 'tanggal_usulan',
                'old' => $skema->tanggal_usulan
            ],
            [
                'nama' => 'Tanggal Review',
                'value' => 'tanggal_review',
                'old' => $skema->tanggal_review
            ],
            [
                'nama' => 'Tanggal Laporan Kemajuan',
                'value' => 'tanggal_laporan_kemajuan',
                'old' => $skema->tanggal_laporan_kemajuan
            ],
            [
                'nama' => 'Tanggal Laporan Akhir',
                'value' => 'tanggal_laporan_akhir',
                'old' => $skema->tanggal_laporan_akhir
            ],
            [
                'nama' => 'Tanggal Publikasi',
                'value' => 'tanggal_publikasi',
                'old' => $skema->tanggal_publikasi
            ]
        ];

        return view('admin.skema-edit', compact('judul', 'link', 'skema', 'form_tanggal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SkemaPenelitian  $skemaPenelitian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:75',
            'tanggal_usulan' => 'required|date',
            'tanggal_review' => 'required|date',
            'tanggal_laporan_kemajuan' => 'required|date',
            'tanggal_laporan_akhir' => 'required|date',
            'tanggal_publikasi' => 'required|date',
            'tanggal_tutup' => 'required|date',
            'dana_maksimal' => 'required|numeric'
        ]);

        SkemaPenelitian::whereId($id)->update($validatedData);
        
        return redirect()->route('skema.index')->with('successpenelitian', 'Skema penelitian berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SkemaPenelitian  $skemaPenelitian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skemaPenelitian = SkemaPenelitian::findOrFail($id);
        $skemaPenelitian->delete();

        return redirect()->route('skema.index')->with('successpenelitian', 'Skema penelitian berhasil dihapus');
    }
}
