<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Dosen;
use App\ListRole;
use App\User;
use App\SkemaPenelitian;
use App\SkemaPengabdian;
use App\Penelitian;
use App\Pengabdian;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        return view('home.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function findId($id)
    {
        $result = Dosen::findId($id);
    }

    function getRole($id)
    {
        // Untuk mengambil Data User
        $user = User::userData($id);
        // Untuk mengambil Data Role sesuai User
        $role = User::userRole($id);

        // Konversi ke Array
        $data[] = $user->toArray();
        $data[] = $role->toArray();
        // var_dump(json_encode($data));
        return json_encode($data);
    }

    function getDosen($id)
    {
        // Untuk mengambil Data User
        $datadosen = Dosen::whereId($id);

        $dosen = $datadosen->first();

        // Konversi ke Array
        $data[] = $dosen->toArray();
        
        return json_encode($data);
    }

    function getskemapenelitian($id)
    {
        // Untuk mengambil Data User
        $skema = SkemaPenelitian::whereId($id)->first();

        // Konversi ke Array
        $data[] = $skema->toArray();
        
        return json_encode($data);
    }

    function getskemapengabdian($id)
    {
        // Untuk mengambil Data User
        $skema = SkemaPengabdian::whereId($id)->first();

        // Konversi ke Array
        $data[] = $skema->toArray();
        
        return json_encode($data);
    }    

    public function getUsulanPenelitian($id)
    {
        $penelitian = Penelitian::whereId($id)->first();

        $data[] = $penelitian->toArray();

        return json_encode($data);
        // echo $data;
    }

    public function getUsulanPengabdian($id)
    {
        $pengabdian = Pengabdian::whereId($id)->first();

        $data[] = $pengabdian->toArray();

        return json_encode($data);
        // echo $data;
    }
}
