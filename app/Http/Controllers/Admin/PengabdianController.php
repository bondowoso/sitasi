<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Pengabdian;
use App\SkemaPengabdian;
use Illuminate\Http\Request;

class PengabdianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|pimpinan|pengabdian');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tes($nidn)
    {  
        $data = SkemaPengabdian::getSkemaByNIDN($nidn);
        dd($data);
    }

    public function index()
    {
        $pengabdian = Pengabdian::getPengabdian();
        if ($pengabdian) {
            $pengabdian = true;
        }

        return view('admin.pengabdian', compact('pengabdian'));
    }

    function getAllUsulan()
    {
        $data = Pengabdian::getAllUsulan();

        return view('admin.usulan-pengabdian', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengabdian = Pengabdian::firstPengabdian($id);

        return view('#', compact('pengabdian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'status' => 'required'
        ]);
        $pengabdian = PengabdianRiwayat::update($validatedData)->where('id_pengabdian', $id);
        
        return redirect()->route(Auth::user()->getActiveRole(Auth::user()->id) . 'Penilaian.index')->with('success', 'Status usulan berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function firstPengabdian($id)
    {
        return $pengabdian = Pengabdian::firstPengabdian(1);

        // Konversi ke Array
        $data[] = $pengabdian->toArray();
        // var_dump(json_encode($data));
        return json_encode($data);
    }
}
