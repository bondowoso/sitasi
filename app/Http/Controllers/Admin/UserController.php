<?php

namespace App\Http\Controllers\Admin;

use App\Dosen;
use App\Http\Controllers\Controller;
use App\ListRole;
use App\RoleUser;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Untuk mengambil Data Role sesuai User
        $data = User::roleData();
        
        return view('admin.user', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // dd($request->admin)->where('id', $request->id);
    }

    public function updateuser(Request $request)
    {
        $id_dosen = $request->id_dosen;

        $destroy_listrole = ListRole::where('user_id', $request->id_dosen)->delete();
        // $destroy_roleuser = RoleUser::where('user_id', $request->id_dosen)->delete();
        $store_listrole = new ListRole();
        $roles = [];
        foreach ($request->role as $role) {
            $data = [];
            $data['user_id'] = $request->id_dosen;
            $data['role_id'] = $role;
            $roles[]=$data;
        }
        $store_listrole->insert($roles);

        $update_roleuser = RoleUser::updateRoleUser($id_dosen);

        return redirect()->route('user.index')->with('success', 'Role berhasil di ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    function getdosen($id)
    {
        // Untuk mengambil Data User
        $datadosen = Dosen::find($id);

        $dosen = $datadosen->first();

        // Konversi ke Array
        $data[] = $dosen->toArray();
        // var_dump(json_encode($data));
        return json_encode($data);
    }
    
    public function dosen()
    {
        // Untuk mengambil Data Role sesuai User
        $dosen = Dosen::get();
        // var_dump($dosen);
        return view('admin.dosen', compact('dosen'));
    }

    function resetPassword($id)
    {
        User::resetPassword($id);

        return redirect()->route('user.index')->with('success', 'Password berhasil direset');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'id_dosen' => 'required|integer',
            'password' => 'required'
        ]);
        $id_dosen = $request->get('id_dosen2');
        $password = $request->get('password');

        echo $id_dosen, $password;

        // $update_password = User::updatePassword($id_dosen, $password);

        // return redirect()->route('user.index')->with('success', 'Password berhasil diubah');
    }
}
