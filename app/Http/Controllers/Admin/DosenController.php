<?php

namespace App\Http\Controllers\Admin;

use App\Dosen;
use App\Role;
use App\ListRole;
use App\User;
use App\UserRole;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\JabatanAkademik;

class DosenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Untuk mengambil Data Role sesuai User
        $dosen = Dosen::all();

        return view('admin.dosen', compact('dosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatan_akademik = JabatanAkademik::get();

        return view('admin.dosen-create', compact('jabatan_akademik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nidn' => 'numeric',
            'nama' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'ktp' => 'required|numeric',
            'telepon' => 'required|numeric',
            'hp' => 'required|numeric',
            'email' => 'required|email',
            'website_personal' => 'required',
            'jabatan_fungsional' => 'required',
            'sinta_id' => 'required',
            'google_scholar' => 'required'
        ]);
        
        $ceknidn = Dosen::where('nidn', $request->get('nidn'))->first();
        if (isset($ceknidn->nidn)) {
            return back()->with('error', 'Data dosen telah terdaftar');
        } else {
            $nidn = $request->get('nidn');
            $nama = $request->get('nama');
            $alamat = $request->get('alamat');
            $tempat_lahir = $request->get('tempat_lahir');
            $tanggal_lahir = $request->get('tanggal_lahir');
            $ktp = $request->get('ktp');
            $telepon = $request->get('telepon');
            $hp = $request->get('hp');
            $email = $request->get('email');
            $website_personal = $request->get('website_personal');
            $jabatan_fungsional = $request->get('jabatan_fungsional');
            $sinta_id = $request->get('sinta_id');
            $google_scholar = $request->get('google_scholar');
            $status = 1;
            
            if ($request->get('status') == null) {
                $status = 2;
            }
    
            $user_data = Dosen::storeDosen($nidn, $nama, $alamat, $tempat_lahir, $tanggal_lahir, $ktp, $telepon, $hp, $email, $website_personal, $jabatan_fungsional, $sinta_id, $google_scholar, $status);
            $user = User::storeUser($nidn);
            $list_role = ListRole::storeListRole();
    
            return redirect()->route('dosen.index')->with('success', 'Data dosen telah ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen = Dosen::findOrFail($id);
        $jabatan_akademik = JabatanAkademik::get();

        return view('admin.dosen-edit', compact('dosen', 'jabatan_akademik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function importExcel()
    {
        $this->validate($request, [
            'select_file'   => 'required|mimes:xls,xlsx'
        ]);
        
        Excel::import(new DosenImport, request()->file('select_file'));

        return redirect('/')->with('success', 'Data dosen berhasil ditambahkan');
    }
}
