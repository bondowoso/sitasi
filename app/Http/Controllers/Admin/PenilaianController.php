<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Penelitian;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|pimpinan');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penelitian = Penelitian::getPenelitian();
        if ($penelitian) {
            $penelitian = true;
        }

        return view('admin.penilaian', compact('penelitian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penelitian = Penelitian::firstPenelitian($id);

        return view('#', compact('penelitian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'status' => 'required'
        ]);
        $penelitian = PenelitianRiwayat::update($validatedData)->where('id_penelitian', $id);
        
        return redirect()->route(Auth::user()->getActiveRole(Auth::user()->id) . 'Penilaian.index')->with('success', 'Status usulan berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function firstPenelitian($id)
    {
        return $penelitian = Penelitian::firstPenelitian(1);

        // Konversi ke Array
        $data[] = $penelitian->toArray();
        // var_dump(json_encode($data));
        return json_encode($data);
    }
}
