<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SkemaPengabdian;
use Illuminate\Http\Request;

class SkemaPengabdianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $judul = "Skema Pengabdian";
        $link = "skemapengabdian.store";
        $form_tanggal = [
            [
                'nama' => 'Tanggal Usulan',
                'value' => 'tanggal_usulan'
            ],
            [
                'nama' => 'Tanggal Review',
                'value' => 'tanggal_review'
            ],
            [
                'nama' => 'Tanggal Laporan Kemajuan',
                'value' => 'tanggal_laporan_kemajuan'
            ],
            [
                'nama' => 'Tanggal Laporan Akhir',
                'value' => 'tanggal_laporan_akhir'
            ],
            [
                'nama' => 'Tanggal Publikasi',
                'value' => 'tanggal_publikasi'
            ]
        ];
        
        return view('admin.skema-create', compact('judul', 'link', 'form_tanggal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:75',
            'tanggal_usulan' => 'required|date',
            'tanggal_review' => 'required|date',
            'tanggal_laporan_kemajuan' => 'required|date',
            'tanggal_laporan_akhir' => 'required|date',
            'tanggal_publikasi' => 'required|date',
            'dana_maksimal' => 'required|numeric',
            'status' => 'required'
        ]);

        $skemaPengabdian = SkemaPengabdian::create($validatedData);
   
        return redirect()->route('skema.index')->with('successpengabdian', 'Skema pengabdian berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SkemaPengabdian  $skemaPengabdian
     * @return \Illuminate\Http\Response
     */
    public function show(SkemaPengabdian $skemaPengabdian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SkemaPengabdian  $skemaPengabdian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $judul = "Skema Pengabdian";
        $link = "skemapengabdian.update";
        $skema = SkemaPengabdian::where('id', $id)->first();
        $form_tanggal = [
            [
                'nama' => 'Tanggal Usulan',
                'value' => 'tanggal_usulan',
                'old' => $skema->tanggal_usulan
            ],
            [
                'nama' => 'Tanggal Review',
                'value' => 'tanggal_review',
                'old' => $skema->tanggal_review
            ],
            [
                'nama' => 'Tanggal Laporan Kemajuan',
                'value' => 'tanggal_laporan_kemajuan',
                'old' => $skema->tanggal_laporan_kemajuan
            ],
            [
                'nama' => 'Tanggal Laporan Akhir',
                'value' => 'tanggal_laporan_akhir',
                'old' => $skema->tanggal_laporan_akhir
            ],
            [
                'nama' => 'Tanggal Publikasi',
                'value' => 'tanggal_publikasi',
                'old' => $skema->tanggal_publikasi
            ]
        ];

        return view('admin.skema-edit', compact('judul', 'link', 'skema', 'form_tanggal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SkemaPengabdian  $skemaPengabdian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:75',
            'tanggal_usulan' => 'required|date',
            'tanggal_review' => 'required|date',
            'tanggal_laporan_kemajuan' => 'required|date',
            'tanggal_laporan_akhir' => 'required|date',
            'tanggal_publikasi' => 'required|date',
            'tanggal_tutup' => 'required|date',
            'dana_maksimal' => 'required|numeric'
        ]);

        SkemaPengabdian::whereId($id)->update($validatedData);
        
        return redirect()->route('skema.index')->with('successpengabdian', 'Skema pengabdian berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SkemaPengabdian  $skemaPengabdian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skemaPengabdian = SkemaPengabdian::findOrFail($id);
        $skemaPengabdian->delete();

        return redirect()->route('skema.index')->with('successpengabdian', 'Skema pengabdian berhasil dihapus');
    }
}
