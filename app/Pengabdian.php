<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengabdian extends Model
{
    protected $table = "pengabdian";

    protected $fillable = ['id_dosen', 'judul', 'skema', 'tahun_usulan', 'tahun_pengabdian', 'abstrak', 'keyword', 'rumpun_ilmu', 'nama_mitra', 'program_studi', 'kaprodi', 'kalppm', 'step', 'status', 'reviewer1', 'reviewer2'];
    
    protected $hidden = ['created_at', 'updated_at'];

    public static function countUsulan()
    {
        $data = Pengabdian::count();

        return $data;
    }

    public static function getAllUsulan()
    {
        $usulan = DB::table('pengabdian AS p')->select('p.id','judul','tahun_usulan','tahun_pengabdian','pr.step','ud.nama','ud.nidn')
                ->leftjoin('pengabdian_riwayat AS pr','pr.id_pengabdian','p.id')
                ->leftjoin('user_data AS ud','ud.nidn','p.id_dosen')
                ->get();
        $data = [];
        foreach ($usulan as $key => $u) {
            $anggota = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                        ->join('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                        ->where('pengabdian_anggota.id_pengabdian', $u->id)
                        ->get();
            $ketua = Dosen::where('nidn',$u->id_dosen)->first();
            $data[$key]['usulan'] = $u;
            $data[$key]['anggota'] = $anggota;
        }
    }
    
    public static function getOnGoingPengabdian($nidn, $tahun){}

    public static function getPengabdian()
    {
        $pengabdian = Pengabdian::all();
        if ($pengabdian->isNotEmpty()) {
            foreach ($pengabdian as $key => $value) {
                if ($value->step == 5) {
                    $id = $value->id;
        
                    $data[$key] = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                                                ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.reviewer1 as reviewer', 'pengabdian.step')
                                                ->firstWhere('pengabdian.id', $id);
        
                    $data[$key]['skema'] = Pengabdian::join('skema_pengabdian', 'pengabdian.skema', 'skema_pengabdian.id')
                                                        ->select('pengabdian.skema as id', 'skema_pengabdian.nama as nama')
                                                        ->firstWhere('skema_pengabdian.id', $value->skema);

                    $data[$key]['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                                ->leftJoin('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                                ->where('pengabdian_anggota.id_pengabdian', $id)
                                                                ->get();

                    $data[$key]['reviewer'] = Pengabdian::join('user_data', 'pengabdian.reviewer1', 'user_data.nidn')
                                                            ->select('pengabdian.reviewer1 as nidn', 'user_data.nama')
                                                            ->firstWhere('user_data.nidn', $value->reviewer1);
            
                    $data[$key]['status'] = PengabdianRiwayat::select('step', 'status')
                                                                ->where('id_pengabdian', $id)
                                                                ->latest()
                                                                ->first();
        
                    $data[$key]['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                                ->where('id_pengabdian', $id)
                                                                ->get();
        
                    $data[$key]['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                                            ->firstWhere('id_pengabdian', $id);
                }
            }
            return $data;
        }
    }

    public static function getPengabdianByNidn($nidn)
    {
        $pengabdian = Pengabdian::where('id_dosen', $nidn)->get();
        if ($pengabdian->isNotEmpty()) {
            foreach ($pengabdian as $key => $value) {
                $id = $value->id;

                $data[$key] = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                                            ->join('skema_pengabdian','skema_pengabdian.id','pengabdian.skema')
                                            ->leftJoin('pengabdian_riwayat','pengabdian_riwayat.id_pengabdian','pengabdian.id')
                                            ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.step', 'skema_pengabdian.nama as skema', 'pengabdian_riwayat.step as progress')
                                            ->firstWhere('pengabdian.id', $id);

                $data[$key]['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                            ->leftJoin('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                            ->where('pengabdian_anggota.id_pengabdian', $id)
                                                            ->get();

                $data[$key]['status'] = PengabdianRiwayat::select('step', 'status')
                                                            ->where('id_pengabdian', $id)
                                                            ->latest()
                                                            ->first();

                $data[$key]['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                            ->where('id_pengabdian', $id)
                                                            ->get();

                $data[$key]['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                                        ->firstWhere('id_pengabdian', $id);
            }
            
            return $data;
        }
    }

    public static function getPengabdianByReviewer($nidn)
    {
        $pengabdian = Pengabdian::where('reviewer1', $nidn)->get();
        if ($pengabdian->isNotEmpty()) {
            foreach ($pengabdian as $key => $value) {
                $id = $value->id;

                $data[$key] = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                                            ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.reviewer1 as reviewer', 'pengabdian.step')
                                            ->firstWhere('pengabdian.id', $id);

                $data[$key]['skema'] = Pengabdian::join('skema_pengabdian', 'pengabdian.skema', 'skema_pengabdian.id')
                                                    ->select('pengabdian.skema as id', 'skema_pengabdian.nama as nama')
                                                    ->firstWhere('skema_pengabdian.id', $value->skema);
    
                $data[$key]['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                            ->leftJoin('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                            ->where('pengabdian_anggota.id_pengabdian', $id)
                                                            ->get();

                $data[$key]['reviewer'] = Pengabdian::join('user_data', 'pengabdian.reviewer1', 'user_data.nidn')
                                                        ->select('pengabdian.reviewer1 as nidn', 'user_data.nama')
                                                        ->firstWhere('user_data.nidn', $value->reviewer1);
    
                $data[$key]['status'] = PengabdianRiwayat::select('step', 'status')
                                                            ->where('id_pengabdian', $id)
                                                            ->latest()
                                                            ->first();

                $data[$key]['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                            ->where('id_pengabdian', $id)
                                                            ->get();

                $data[$key]['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                                        ->firstWhere('id_pengabdian', $id);
            }
            
            return $data;
        }
    }

    public static function firstPengabdian($id)
    {
        $data = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                            ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.step')
                            ->firstWhere('pengabdian.id', $id);

        $data['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                ->leftJoin('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                ->where('pengabdian_anggota.id_pengabdian', $id)
                                                ->get();

        $data['status'] = PengabdianRiwayat::select('step', 'status')
                                                ->where('id_pengabdian', $id)
                                                ->latest()
                                                ->first();

        $data['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_pengabdian', $id)
                                                ->get();

        $data['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                        ->firstWhere('id_pengabdian', $id);
        
        return $data;
    }

    public static function firstPengabdianByNidnWithSkema($nidn, $skema)
    {
        $data = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                            ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.nama_mitra', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.step', 'pengabdian.skema')
                            ->orderByDesc('pengabdian.id')
                            ->where('skema', $skema)
                            ->firstWhere('pengabdian.id_dosen', $nidn);

        if (isset($data)) {
            $data['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                    ->join('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                    ->where('pengabdian_anggota.id_pengabdian', $data->id)
                                                    ->get();
    
            $data['status'] = PengabdianRiwayat::select('step', 'status')
                                                ->where('id_pengabdian', $data->id)
                                                ->latest()
                                                ->first();
    
            $data['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_pengabdian', $data->id)
                                                ->get();
    
            $data['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                            ->firstWhere('id_pengabdian', $data->id);
        }

        return $data;
    }

    public static function latestPengabdianByNidn($nidn)
    {
        $data = Pengabdian::join('user_data', 'pengabdian.id_dosen', 'user_data.nidn')
                            ->select('pengabdian.id', 'pengabdian.id_dosen as nidn', 'user_data.nama', 'pengabdian.judul', 'pengabdian.tahun_usulan', 'pengabdian.tahun_pengabdian', 'pengabdian.abstrak', 'pengabdian.keyword', 'pengabdian.rumpun_ilmu', 'pengabdian.program_studi', 'pengabdian.kaprodi', 'pengabdian.kalppm', 'pengabdian.step','pengabdian.skema')
                            ->orderByDesc('pengabdian.id')
                            ->firstWhere('pengabdian.id_dosen', $nidn);

        if (isset($data)) {
            $data['anggota'] = PengabdianAnggota::select('user_data.nidn', 'user_data.nama')
                                                    ->join('user_data', 'pengabdian_anggota.id_dosen', 'user_data.nidn')
                                                    ->where('pengabdian_anggota.id_pengabdian', $data->id)
                                                    ->get();
    
            $data['status'] = PengabdianRiwayat::select('step', 'status')
                                                ->where('id_pengabdian', $data->id)
                                                ->latest()
                                                ->first();
    
            $data['berkas'] = PengabdianRiwayat::select('berkas', 'keterangan', 'komentar')
                                                ->where('id_pengabdian', $data->id)
                                                ->get();
    
            $data['dana'] = PengabdianRab::select('usulan_dana', 'sumber_dana_lain')
                                            ->firstWhere('id_pengabdian', $data->id);
        }

        return $data;
    }
}
