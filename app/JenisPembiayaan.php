<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPembiayaan extends Model
{
    protected $table = "jenis_pembiayaan";

    protected $fillable = ['nama', 'aktif'];
}
