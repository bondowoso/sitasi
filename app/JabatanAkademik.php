<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanAkademik extends Model
{
    protected $table = "jabatan_akademik";

    protected $fillable = ['nama', 'aktif'];

    public static function getJabatanAkademik($user_id)
    {
        $jabatan_akademik = JabatanAkademik::select('jabatan_akademik.nama as nama')
            ->join('user_data', 'jabatan_akademik.id', '=', 'user_data.jabatan_fungsional')
            ->where('user_data.id', $user_id)
            ->first();

        return $jabatan_akademik;
    }
}
